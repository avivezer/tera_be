FROM node:12.16.2
## Make sure, your ARG comes after your FROM. See:
#See: https://docs.docker.com/engine/reference/builder/#understand-how-arg-and-from-interact
ARG ContainerNODENV

# #variables won't be interpretad inside '', so use "" instead.
RUN echo "ContainerNODENV is ${ContainerNODENV}"


# Create app directory
WORKDIR /usr/src/app

# Install app dependencies
# A wildcard is used to ensure both package.json AND package-lock.json are copied
# where available (npm@5+)
COPY package*.json ./

#RUN npm config set registry https://registry.npmjs.org/

RUN npm install
# If you are building your code for production
# RUN npm ci --only=production

# Bundle app source
COPY . .


#ENV PORT 80
# - This instruction will set an environment variable named PORT(in the Docker container)
#to the value 80. Although EB will set Docker container environment variables
# to the values configured in the normal way,
#  it is useful to “hard-code” PORT to 80 here in the Dockerfile
#   because EB requires Docker containers to have an environment variable named PORT set to 80.


## this pass to runabble environment (like process.env.foo)
## package.json could be override it (like  "start_prod": "NODE_ENV=production node app.js",
## ENV NODE_ENV production
## PASSED FROM BUILD
## https://docs.docker.com/engine/reference/builder/#arg
## can also pass env variables(value/from) ecs console task defenition

ENV NODE_ENV=${ContainerNODENV}

EXPOSE 80

CMD [ "npm", "run", "start_docker" ]


