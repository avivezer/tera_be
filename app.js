const ENV_CONFIG = require('./configurations/env_config');
const express = require('express');
const router = require('express-promise-router')();
const bodyParser = require('body-parser');
const AWS = require('aws-sdk');
AWS.config.update({accessKeyId:ENV_CONFIG.ENV_AWS_ACCESS_KEY_ID ,
    secretAccessKey: ENV_CONFIG.ENV_AWS_ACCESS_KEY_SECRET,
    region: ENV_CONFIG.ENV_AWS_REGION,
});

const DB = require('./libs/db');

const httpContext = require('express-http-context');
const uuid = require('uuid');
require('./libs/helperFunctions');
const Helmet = require('helmet')
const cors = require('cors')
const {reqResLogger, reqResErrorLogger , Logger } = require('./libs/mlogger');

// const https = require('https');
const http = require('http');
// const fs = require('fs');

//get local time zone
const moment = require('moment');
console.log("current server init time: "  + moment.utc().format());

// set up express app
const app = express();

// Enable if you're behind a reverse proxy (Heroku, Bluemix, AWS ELB, Nginx, etc) (express-rate-limit)
// see https://expressjs.com/en/guide/behind-proxies.html
app.set('trust proxy', 1);

// secure your Express apps by setting various HTTP headers
app.use(Helmet({referrerPolicy:{ policy: 'no-referrer' }}))

// order of middleware is impotent , bodyParser should be first

// use body-parser middleware  : // parse application/x-www-form-urlencoded
app.use(  bodyParser.json({limit: "100kb"}));
// app.use(bodyParser.urlencoded({
//     limit: "10mb",
//     extended: true,
//     parameterLimit:1000
// }));

// app.use(require('method-override')());  // for express winston


// Use any third party middleware that does not need access to the context before here,
// all code from here on has access to the same context for each request
app.use(httpContext.middleware);
app.use(function(req, res, next) {
    let val  = uuid.v1()
    httpContext.set('reqId',val);
    req.reqId = val
    next();
});

// express-winston logger makes sense BEFORE the router
app.use(reqResLogger);

//frontend that not from port 80, the browser sends firest an option (preflight request), so set heather for that and
const allowCrossDomain = function (req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', "Origin, Content, Content-Type, X-Auth-Token, " +
        "access-control-allow-headers, access-control-allow-methods, access-control-allow-origin, content-type, " +
        "Access-Control-Allow-Headers, Authorization, authorization, X-Requested-With, Accept");

    next();
};
//frontend that not from port 80, the browser sends firest an option (preflight request), so set heather for that and
const allowCrossDomainerr = function (err, req, res, next) {
    res.header('Access-Control-Allow-Origin', "*");
    res.header('Access-Control-Allow-Methods', 'GET, PUT, POST, PATCH, DELETE, OPTIONS');
    res.header('Access-Control-Allow-Headers', "Origin, Content, Content-Type, X-Auth-Token, " +
        "access-control-allow-headers, access-control-allow-methods, access-control-allow-origin, content-type, " +
        "Access-Control-Allow-Headers, Authorization, authorization, X-Requested-With, Accept");

    next(err);
};
app.use(cors({credentials: true, origin: true , maxAge: 86400 })) // max age for caching Access-Control-Max-Age option preFlight result
app.use('/api', allowCrossDomain);  // use this after helmet
app.use('/api', allowCrossDomainerr);  // use this after helmet


// aws health control check
app.use( router.get("/hc" , (req,res,next)=> res.status(200).send()));

// initialize routes
app.use('/api/v1/webapp', require('./routes/user_r'));
app.use('/api/v1/webapp', require('./routes/general_Ssystem_admin_r'));
app.use('/api/v1/webapp', require('./routes/doctors_r'));

app.use('/api/v1/devices', require('./routes/device_r')); //fixme remove from webapp

// express-winston errorLogger makes sense AFTER the router.
app.use(reqResErrorLogger);


// global error handling middleware (last) (for web request)
//explain : we warped all routes with promise , each route rolling the err to the next until
//https://expressjs.com/en/guide/error-handling.html
//https://www.npmjs.com/package/express-promise-router
app.use(require('./routes/errorHandler'))

//listen for requests

//netstat -a   {make sure no service using the port)
const port = 80
var server
if (require.main === module) {
    app.set('port', port);
    server = http.createServer(app)
    server.listen(port, function() {
        console.log('Express server listening on port ' + server.address().port);
    });
    server.on('error', onError);
    server.on('listening', onListening);
    server.on('close', ()=>console.log("server connection close"));
}


// //netstat -a   {make sure no service using the port)
// if (require.main === module)
// https.createServer(options, app).listen(443, function (err,bb) {
//   if(!err) {
//       Logger.info('now listening for httpS requests');
//   }
// });

if (process.env.NODE_ENV !== "production") {

    var blocked = require('blocked');
    blocked(function(ms){

        console.log(`BLOCKED FOR ${ ms | 0}ms`);
    } ,  {threshold:40, interval: 80});   //interval determines the frequency with which the event loop (that running this blocked function) is checked in ms.
                                                    // the dela is the delayed execution of this function in the interval defined
     //or //https://github.com/AlbertoFdzM/express-list-endpoints
    let routeList = require("express-routes-catalogue")
    routeList.default.web(  // For web output
        app,
        "/api/v1/webapp/general/routeList"
    );

    const branchName = require('current-git-branch');
    console.log(`branch name: ${branchName()}`);

}

process.on('uncaughtException', function (err) {
    console.error(err.stack);
    console.log("Node NOT Exiting...");
});

process.on('exit', function () {
    console.log('About to exit, waiting for remaining connections to complete');
});


// let extIP = require('ext-ip')();
// extIP.get().then(ip => {
//     console.log("my extrnal ip: "+ ip);
// })
//     .catch(err => {
//         console.error(err);
//     });

function onError(error) {
    if (error.syscall !== 'listen') {
        throw error;
    }

    let bind = typeof port === 'string'
        ? 'Pipe ' + port
        : 'Port ' + port;

    // handle specific listen errors with friendly messages
    switch (error.code) {
        case 'EACCES':
            console.error(bind + ' requires elevated privileges');
            process.exit(1);
            break;
        case 'EADDRINUSE':
            console.error(bind + ' is already in use');
            process.exit(1);
            break;
        default:
            throw error;
    }
}

/**
 * Event listener for HTTP server "listening" event.
 */

function onListening() {
    let addr = server.address();
    let bind = typeof addr === 'string'
        ? 'pipe ' + addr
        : 'port ' + addr.port;
    console.log('server Listening on ' + bind);
}

const {version} = require('./package.json');
console.log("EyeSwift Webapp BackEnd app version " + version)
module.exports = {
    info: {
        AppName: 'EyeSwift Webapp BackEnd',
        CurrentAPIVersion: version
    },
    app:app //export app for server wrapping
};

