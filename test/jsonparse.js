



// const loadJsonFile = require('load-json-file');
// let obj
// try {
//     obj = loadJsonFile('uploads_processed/ans.json').then(json => {
//         // `json` contains the parsed object
//         console.log(json)
//     });
// }
// catch (e) {
//     console.log(e)
// }








var fs = require('fs');
let obj;
//
// //// Sync:
// //  obj = JSON.parse(fs.readFileSync('../uploads_processed/ans.json', 'utf8'));
// //
//
////Async:
fs.readFile('uploads_processed/ans.json', 'utf8', function (err, data) {
    if (err) throw err;
    obj = JSON.parse(data);

    console.log(obj)
});


//https://azimi.me/2015/07/30/non-blocking-async-json-parse.html
// let obg = {
//     test_Time: "10:12",
//     test_Date: "1/2/2019",
//     CallerID: "0506929894",
//     Volume: {
//         value: "200",
//         unit: "ml"
//     },
//     Qmax: {
//         value: "15",
//         unit: "ml/sec"
//     },
//     Qav: {
//         value: "15",
//         unit: "ml/sec"
//     },
//     time_total: {
//         value: "30",
//         unit: "sec"
//     },
//     time_flow: {
//         value: "20",
//         unit: "sec"
//     },
//     OBL_level: "obstructed",
//     remarks: "OK",
//     data_time: {
//         value: [
//             "0",
//             "0.1",
//             "0.2",
//             "0.3",
//             "0.4",
//             "0,5"
//         ],
//         unit: "sec"
//     },
//     Flowrate: {
//         value: [
//             "10",
//             "20",
//             "30",
//             "40",
//             "50",
//             "60"
//         ],
//         unit: "sec"
//     }
// }
//
// obj = JSON.stringify(obg);
//     console.log(obj)
