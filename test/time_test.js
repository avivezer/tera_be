const moment = require('moment');

/**
 * return new iso format string with UTC offset.
 * @param {boolean} [toURI=false] if send in URI Get method, need set to true.
 * @returns {string}
 */
Date.prototype.toIsoString = function(toURI = false) {
    var tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? toURI ? encodeURIComponent('+') : '+' : toURI ? encodeURIComponent('-') : '-',
        pad = function(num) {
            var norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate()) +
        'T' + pad(this.getHours()) +
        ':' + pad(this.getMinutes()) +
        ':' + pad(this.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
};
//Moment's string parsing functions like moment(string) and moment.utc(string) accept offset information if provided, but convert the resulting Moment object to local or UTC time. In contrast, moment.parseZone() parses the string but keeps the resulting Moment object in a fixed-offset timezone with the provided offset in the string.


//// console.log(new Date().toLocaleString()) // local format
// //// console.log(new Date().getTime())  // utc , same as console.log(new Date().valueOf())  // utc
//// var currmanualUTCtime = new Date();
//// var offset = new Date().getTimezoneOffset() // -120   (minuets)
//// console.log(new Date(currmanualUTCtime.valueOf() - offset*60000));


var client_now = moment.parseZone('2018-12-20T15:00:00+05:00');

const time = '14:00';
const [hour, minute] = time.split(':').map(Number);
// var client_future = moment(client_now + 1)   // once i substart it converted to utc , so client_future.format() will be in server time

var client_future = moment(client_now).set({ hour, minute })

let daysOfPhase = client_future.diff(client_now, "days") + 1 // +1 because its floor and counts from 0
// console.log(daysOfPhase)
//
// console.log(  client_future.format() )

// console.log(asign - client_now)  // 3600000 is 1 hour



let x = new Date().toIsoString(false)

let y =  new Date(x).toLocaleTimeString()
// console.log(y); // add this in helper functions

// console.log(dt.getTimezoneOffset()) // -120

// const client_time = moment.parseZone("2018-12-22T23:00:00.0000000 02:00")

let visit0 = moment('2007-01-28T15:00:00Z'); //prev
let visit1 =moment('2007-01-29T10:00:00Z');

let dayDiff = visit1.startOf('day').diff(visit0.startOf('day') , 'days')
console.log(dayDiff)
