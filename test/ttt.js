let a = 1;

if (true) {
    console.log(a);
    // let a = 2;
}

if (true) {
    let a = 2;
    console.log(a);

}

function msleep(n) {
    Atomics.wait(new Int32Array(new SharedArrayBuffer(4)), 0, 0, n);
}
console.log(a);
