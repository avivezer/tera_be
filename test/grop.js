let visits = [
    {
        "_id": "5e87e61082aca532a8c7a088",
        "PatientID": "User08",
        "Visited": true,
        "ClosedDate": "2020-04-04T01:42:44.625Z",
        "CurrentPatientProtocol": [
            {
                "WeighDays": [
                    "SU"
                ],
                "BloodPulseDays": [
                    "SU"
                ],
                "_id": "5e87e61082aca532a8c7a086",
                "DrainTime": 111111111,
                "FillTime": 5,
                "MinFillTemp": 55,
                "MaxFillTemp": 77,
                "Exchanges": [
                    {
                        "ExchangeDaysFrequency": [
                            "SU"
                        ],
                        "ExchangeTime": [
                            "18:00"
                        ],
                        "_id": "5e87e61082aca532a8c7a087",
                        "ExchangeFluid": "dssdf",
                        "ExchangeLiter": 6,
                        "Created_UTC": "2020-04-04T01:42:40.441Z"
                    }
                ],
                "updatedAt": "2020-04-04T01:42:40.430Z",
                "createdAt": "2020-04-04T01:42:40.430Z",
                "Created_UTC": "2020-04-04T01:42:40.442Z"
            }
        ],
        "DeviceID": "Device10",
        "DoctorID": null,
        "Findings": "gsdxdfjkkkkjjjgs",
        "MachineProblem": [],
        "Note": null,
        "OpenDate": "2020-04-04T01:42:40.490Z",
        "Recommendation": "sdgs",
        "Skipped": [],
        "Uf": [],
        "UrineOutput": "5",
        "VisitStatus": null,
        "Weight": [],
        "__v": 0,
        "createdAt": "2020-04-04T01:42:40.486Z",
        "updatedAt": "2020-04-04T01:42:44.626Z"
    },
    {
        "_id": "5e87e61882aca532a8c7a08b",
        "PatientID": "User08",
        "Visited": true,
        "ClosedDate": "2020-04-04T01:42:51.989Z",
        "CurrentPatientProtocol": [
            {
                "WeighDays": [
                    "SU"
                ],
                "BloodPulseDays": [
                    "SU"
                ],
                "_id": "5e87e61882aca532a8c7a089",
                "DrainTime": 111111111,
                "FillTime": 5,
                "MinFillTemp": 55,
                "MaxFillTemp": 77,
                "Exchanges": [
                    {
                        "ExchangeDaysFrequency": [
                            "SU"
                        ],
                        "ExchangeTime": [
                            "18:00"
                        ],
                        "_id": "5e87e61882aca532a8c7a08a",
                        "ExchangeFluid": "dssdf",
                        "ExchangeLiter": 6,
                        "Created_UTC": "2020-04-04T01:42:48.893Z"
                    }
                ],
                "updatedAt": "2020-04-04T01:42:48.892Z",
                "createdAt": "2020-04-04T01:42:48.892Z",
                "Created_UTC": "2020-04-04T01:42:48.894Z"
            }
        ],
        "DeviceID": "Device10",
        "DoctorID": null,
        "Findings": "gsdxdfjkkkkjjjgs",
        "MachineProblem": [],
        "Note": null,
        "OpenDate": "2020-04-04T01:42:48.907Z",
        "Recommendation": "sdgs",
        "Skipped": [],
        "Uf": [],
        "UrineOutput": "5",
        "VisitStatus": null,
        "Weight": [],
        "__v": 0,
        "createdAt": "2020-04-04T01:42:48.906Z",
        "updatedAt": "2020-04-04T01:42:51.990Z"
    },
    {
        "_id": "5e87e680bcac5a495c33f8b8",
        "Visited": false,
        "ClosedDate": null,
        "Note": null,
        "VisitStatus": null,
        "CurrentPatientProtocol": [],
        "Findings": null,
        "Recommendation": null,
        "UrineOutput": null,
        "PatientID": "User08",
        "OpenDate": "2020-04-06T01:44:32.206Z",
        "Weight": [
            {
                "_id": "5e87e8f2e70be617188949fe",
                "Weight": "50",
                "DateUTC": "2020-04-04T01:54:58.754Z"
            }
        ],
        "Skipped": [],
        "MachineProblem": [],
        "Uf": [],
        "createdAt": "2020-04-04T01:44:32.222Z",
        "updatedAt": "2020-04-04T01:54:58.748Z",
        "__v": 0,
        "DeviceID": "Device10",
        "DoctorID": "User04"
    },
    {
        "_id": "5e87e680bcac5a495c33f8b8",
        "Visited": false,
        "ClosedDate": "2020-04-05T02:44:32.206Z",
        "Note": null,
        "VisitStatus": null,
        "CurrentPatientProtocol": [],
        "Findings": null,
        "Recommendation": null,
        "UrineOutput": null,
        "PatientID": "User08",
        "OpenDate": "2020-04-05T01:44:32.206Z",
        "Weight": [
            {
                "_id": "5e87e8f2e70be617188949fe",
                "Weight": "50",
                "DateUTC": "2020-04-04T01:54:58.754Z"
            }
        ],
        "Skipped": [],
        "MachineProblem": [],
        "Uf": [],
        "createdAt": "2020-04-04T01:44:32.222Z",
        "updatedAt": "2020-04-04T01:54:58.748Z",
        "__v": 0,
        "DeviceID": "Device10",
        "DoctorID": "User04"
    }
]



// this gives an object with dates as keys
// const groups = visits.reduce((groups, visit) => {
//
//     const date =visit.ClosedDate ?  visit.ClosedDate.split('T')[0] : "open"
//     if (!groups[date]) {
//         groups[date] = [];
//     }
//     groups[date].push(visit);
//     return groups;
// }, {});

let sortedVisits = visits.sort(function(a,b){
    return new Date(b.OpenDate) - new Date(a.OpenDate); // the end date is always first because the start date is also first
});

//  array format
const groupArrays = sortedVisits.map((visit) => {

    return {
        startDate: visit.OpenDate.split('T')[0],
        endDate : visit.ClosedDate.split('T')[0],
        visits: visit
    };
});

console.log(groupArrays);
