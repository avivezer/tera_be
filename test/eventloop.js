// var open = false;
//
// setTimeout(function() {
//     open = true
// }, 50)
//
// async function f() {
//
//     console.log('wait2');
//     return 1
// }
//
// async function f1() {
//     while (!open) {
//         await f()
//         console.log('wait');
//         await f()
//     }
// }
//
// f1().then()
// console.log('open sesame');


// #################

let done = false;

setTimeout(() => {
    done = true
}, 1000);

const eventLoopQueue = () => {
    return new Promise(resolve => {

        console.log('event loop');
        resolve();

    });
};

const eventLoopQueue2 = async () => {
    setImmediate(() => {
        console.log('event loop2');

    });
}


const eventLoopQueue3 = () => {
    return new Promise(resolve => {
            setTimeout(() => {
                console.log('event loop that finish');
                resolve(); //beacuse the promise take time to resolve it turn to pending and return to event loop
            }, 100)
            console.log('event loop that finish2');
        }
    );
};

const eventLoopQueue33 = () => {
    return new Promise(resolve => {
            setTimeout(() => {
                console.log('event loop');

            }, 100);
            resolve()
        }
    );
};


const sleep = (waitTimeInMs) => new Promise(resolve => setTimeout(resolve, waitTimeInMs));

const eventLoopQueue4 = async () => {
     setTimeout(() => {
        console.log('event loop');

    } , 100)
    await sleep(110); // sleep

}


const eventLoopQueue5 = () => {
    return new Promise(resolve => {
            console.log('event loop');
            resolve();
        });
};

const run = async () => {
    while (!done) {
        console.log('loop');
        console.log( await eventLoopQueue4());
        console.log('loop2');
        // console.log(await eventLoopQueue4());  //        eventLoopQueue3().then();
    }
}

run().then(() => console.log('Done'));
