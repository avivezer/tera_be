'use strict';

///https://github.com/sequelize/cli/blob/master/docs/README.md

const fs = require('fs');
const path = require('path');
const {Sequelize , DataTypes , Transaction } = require('sequelize');
const basename = path.basename(__filename);
const env = process.env.NODE_ENV || 'development';

////The keys of the objects (e.g. "development") are used on model/index.js for matching process.env.NODE_ENV
const config = require(__dirname + '/../configurations/database_config.js')[env];
if (!config)
  throw new Error("sequelize needs a database_config.js")

// config.isolationLevel = Transaction.ISOLATION_LEVELS.SERIALIZABLE
const db = {
  PII:undefined ,
  Users:undefined ,
  Sites:undefined ,
  Counters:undefined,
  EyeCareProviders:undefined,
  Visits:undefined,
  Devices:undefined,
  Patients:undefined,
  Treatments:undefined,
  SystemVersions:undefined,
  DeviceErrors:undefined,
  PendingUsers:undefined,
  Customers:undefined
};

let sequelize;
if (config.use_env_variable) {

  /// will connect to database
  sequelize = new Sequelize(process.env[config.use_env_variable], config);
} else {
  sequelize = new Sequelize(config.database, config.username, config.password, config);
}

fs
  .readdirSync(__dirname)
  .filter(file => {
    return (file.indexOf('.') !== 0) && (file !== basename) && (file.slice(-3) === '.js');
  })
  .forEach(file => {
    const model = require(path.join(__dirname, file));
    const Model = model(sequelize, DataTypes);
      db[Model.name] = Model;
  });

Object.keys(db).forEach(modelName => {
  if (db[modelName].associate) {
    db[modelName].associate(db);
  }
});

db.sequelize = sequelize;
db.Sequelize = Sequelize;

/// cached , from here get sequelize instance
module.exports = db;
