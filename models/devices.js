'use strict';
module.exports = (sequelize, DataTypes) => {
  const device = sequelize.define('Devices', {

    DeviceID: {
      type : DataTypes.STRING,
      allowNull: false,
      primaryKey: true   /// can also be referenced, references: {       model: Bar, key: 'id',}
    },

    SerialNumber: {
      type : DataTypes.STRING,
      allowNull: false,
      unique: true
    },

    OSVersion: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    SoftwareVersion: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    FirmwareVersion: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    TeamviewerID: {
      type : DataTypes.STRING,
      allowNull: false,
    },



    // UpdateRequired   ///0-No update required,1-Windows update,2-CS update,3- ET update

    SystemStatus : { /// errors  OK” , “ET malfunction”
      type: DataTypes.STRING, 
      allowNull: false
    },



    // ESVersion  : DataTypes.STRING,
    // WindowsVersion : DataTypes.STRING,
    // ETFirmwareVersion: DataTypes.STRING,
    // EmitterFirmwareVersion: DataTypes.STRING,
    // TVVersion: DataTypes.STRING,
    //Location: DataTypes.STRING,

    SiteID: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    SiteName: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    CustomerID: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    CustomerName: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    DateOfActivation: { /// DATE // Date of first login by patient // save with Z UTC
      type: DataTypes.DATE,
      allowNull: false
    },

    LastRun: {
      type: DataTypes.DATE,
      allowNull: false
    },

    NumTests: {
      type: DataTypes.INTEGER,
      allowNull: false
    }

  }, {});

  device.prototype.calculateAvailableVersion =async function() {


    let UpdateRequired = {ESVersion : {VersionAvailable : false , NewVersion: null , VersionURL:null} ,
      WindowsVersion:{VersionAvailable : false , NewVersion: null , VersionURL:null} ,
      ETFirmwareVersion:{VersionAvailable : false , NewVersion: null , VersionURL:null},
      TVVersion:{VersionAvailable : false , NewVersion: null , VersionURL:null},
      EmitterFirmwareVersion:{VersionAvailable : false , NewVersion: null , VersionURL:null},
    }

    let systemVersionAvailable =await sequelize.models.SystemVersions.gelLatestVersion()

    if (!systemVersionAvailable)
      return UpdateRequired

    let thisDevice =  this
    if (systemVersionAvailable.ESAPP && thisDevice.ESVersion !== systemVersionAvailable.ESAPP.VersionNO)
      UpdateRequired.ESVersion = {VersionAvailable : true , NewVersion: systemVersionAvailable.ESAPP.VersionNO , VersionURL:systemVersionAvailable.ESAPP.VersionURL};

    if (systemVersionAvailable.WIN && thisDevice.WindowsVersion !== systemVersionAvailable.WIN.VersionNO)
      UpdateRequired.WindowsVersion = {VersionAvailable : true , NewVersion: systemVersionAvailable.WIN.VersionNO , VersionURL:systemVersionAvailable.WIN.VersionURL};

    if (systemVersionAvailable.ET && thisDevice.ETFirmwareVersion !== systemVersionAvailable.ET.VersionNO)
      UpdateRequired.ETFirmwareVersion = {VersionAvailable : true , NewVersion: systemVersionAvailable.ET.VersionNO , VersionURL:systemVersionAvailable.ET.VersionURL};

    if (systemVersionAvailable.EMITTER && thisDevice.EmitterFirmwareVersion !== systemVersionAvailable.EMITTER.VersionNO)
      UpdateRequired.EmitterFirmwareVersion = {VersionAvailable : true , NewVersion: systemVersionAvailable.EMITTER.VersionNO , VersionURL:systemVersionAvailable.EMITTER.VersionURL}

    if (systemVersionAvailable.TV && thisDevice.TVVersion !== systemVersionAvailable.TV.VersionNO)
      UpdateRequired.TVVersion = {VersionAvailable : true , NewVersion: systemVersionAvailable.TV.VersionNO , VersionURL:systemVersionAvailable.TV.VersionURL}

    return UpdateRequired

  };

  device.associate = function(models) {

    device.hasMany(models.DeviceErrors, {foreignKey: 'DeviceID', sourceKey: 'DeviceID' });
    models.DeviceErrors.belongsTo(device, {foreignKey: 'DeviceID', targetKey: 'DeviceID'  , onDelete: 'cascade' ,});
  };
  return device;
};



