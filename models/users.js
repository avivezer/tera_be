'use strict';

const bcrypt = require('bcryptjs');
const moment = require('moment');


module.exports = (sequelize, DataTypes) => {
  const Users = sequelize.define('Users', {

    //surrogate primary key
    UserIDpk: {
      type: DataTypes.STRING,
      primaryKey: true,
    },

    UserID: { // ref to pii
      type : DataTypes.STRING,
      // unique: true,   /// can also reference references: {       model: Bar, key: 'id',} - NO
      allowNull: true,
    },

    Password:{
      type : DataTypes.STRING,
      allowNull: false,
      set(val) { // // cant be async , bcrypt async version uses a thread pool which does not block the main event loop.
        let hashed = bcrypt.hashSync(val.toString(), bcrypt.genSaltSync(10))
        this.setDataValue('Password', hashed);
      }

    },
    Role:{
      type : DataTypes.STRING, /// EyeSwiftSuperAdmin , EyeSwiftAdmin  , Doctor , Patient
      allowNull: false,
      get() {
        const rawValue = this.getDataValue("Role");
        return rawValue ? JSON.parse(rawValue) : null;
      },
      set(val) {
        let permitted = ["EyeSwiftSuperAdmin" , 'EyeSwiftAdmin'  , "Doctor" , "Patient"]
        if (!Array.isArray(val) || !val.every(type => permitted.includes(type)))
          throw new Error("Error Role "+val.toString()+ "is not allowed")
        this.setDataValue('Role', JSON.stringify(val));
      }

    },

    AdminPosition: {
      type : DataTypes.STRING, /// Admin, Call Center
      allowNull: true
    },
    //   get() {
    //     const rawValue = this.getDataValue("AdminPosition");
    //     return rawValue ? JSON.parse(rawValue) : null;
    //   },
    //   set(val) {
    //     let permitted = ["Admin" , "Call Center"]
    //     if (!Array.isArray(val) || !val.every(type => permitted.includes(type)))
    //       throw new Error("Error Role "+val.toString()+ "is not allowed")
    //     this.setDataValue('Role', JSON.stringify(val));
    //   }

    // },

    Enabled:{ /// lock(0) or unlocked(1)
      type : DataTypes.INTEGER,
      allowNull: false,
      defaultValue:1
    },

    /// internal

    TwoFA:{ // for admins
      type : DataTypes.BOOLEAN,
      allowNull: true,
      // defaultValue : () => {
      //   let that = sequelize
      // }
    },

    TwoFaCode:{
      type : DataTypes.STRING,
      allowNull: true,
    },

    LastLoginAt: { /// set in passport
      type : DataTypes.DATE(6),  //utc
      allowNull: true,
    },

    PassNeedsToChangeAt: { /// if there is 2fa, check this after 2fa auth, patient dont need
      type: DataTypes.DATE(6),  //utc
      allowNull: true,
      defaultValue : moment.utc,  //utc
    },

    forgotAt: {
      type : DataTypes.DATE(6),
      allowNull: true,
    },

    FailedLoginAttempts:{
      type : DataTypes.INTEGER,
      allowNull: false,
      defaultValue:0
    },

    language: {
      type : DataTypes.STRING,
      allowNull: false,
      defaultValue:'en'
    }
  }, {
    timestamps: true ,
    indexes:[ {unique: false, fields:['Role']} ],
    getterMethods: {},

    defaultScope: { //still be exposed for create
      attributes: { exclude: ['forgotAt' , 'FailedLoginAttempts' , 'PassNeedsToChangeAt', 'LastLoginAt' ,'TwoFA'] },
    },
    scopes: {
      withAll: {
        attributes: { },
      }
    }

  });

  // class methods
  Users.associate = function(models) {
    // associations can be defined here
  };


// instance methods
  Users.prototype.checkPassword = async function (given_pass) {

    try {
      let ans = await bcrypt.compare(given_pass, this.Password.toString());
      return ans
    } catch (e) {
      console.log(e)
      throw e;
    }
  };

  // not work for upsert
  // Users.addHook('beforeCreate',async function(user, options) {  // Permanent Hooks ,  Local hooks are always run before global hooks
    // after validate
    //   try {
    //     const salt = await bcrypt.genSalt(10);
    //     user.Password = await bcrypt.hash(user.Password, salt);
    //   } catch (e) {
    //     return sequelize.Promise.reject("error passwrd hash")    }
    // })

    // try {
    //   let FAroles = ["EyeSwiftSuperAdmin", 'EyeSwiftAdmin']
    //   user.TwoFA = user.Role.some(
    //       r => FAroles.includes(r));
    //
    // } catch (e) {
    //   return sequelize.Promise.reject("error TwoFA set")
    // }
  // });

  return Users;
};



