'use strict';
module.exports = (sequelize, DataTypes) => {
  const customer = sequelize.define('Customers', {

    CustomerID: {
      type : DataTypes.STRING,
      allowNull: false,
      primaryKey: true   /// can also be referenced, references: {       model: Bar, key: 'id',}
    },

    /// no site admin , only eyeswift admin can control them

    CustomerName   : {
      type : DataTypes.STRING,
      allowNull: false,
      unique: true
    },

    CRMLink : {
      type : DataTypes.STRING,
      allowNull: false,
    },

    // NumSites : {
    //     type : DataTypes.INTEGER,
    //     allowNull: true,
    // },

    // NumOperators : {
    //     type : DataTypes.INTEGER,
    //     allowNull: true,
    // },

    // NumDevices : {
    //     type : DataTypes.INTEGER,
    //     allowNull: true,
    // },

    Enabled :{   /// 0-Lock/on hold, 1-Active
      type : DataTypes.INTEGER,
      allowNull: false,
      defaultValue:1 // active
    },

  }, {});

  customer.associate = function(models) {
    //BelongsTo will add the foreignKey on the source where hasOne will add on the target
    // so the foreign key is in EyeCareProviders , which means EyeCareProviders pointing -> to the Sites , Sites is the primary
    customer.hasMany(models.Sites, {foreignKey: 'CustomerID', sourceKey: 'CustomerID'});
  //    models.DeviceErrors.belongsTo(device, {foreignKey: 'DeviceID', targetKey: 'DeviceID'  , onDelete: 'cascade' ,});

    //sourceKey  referring to a column of the current model ,
    // hasOne association keep the foreign key on the target model
    models.Sites.belongsTo(customer, {foreignKey: 'CustomerID', targetKey: 'CustomerID' , onDelete: 'SET NULL'  });
    // targetKey  referring to a column of the target model
    // This creates a foreign key called `SiteID` in the source model (EyeCareProviders)
    // which references the targetKey `SiteID` field from the target model (site).
    customer.hasMany(models.EyeCareProviders, {foreignKey: 'CustomerID', sourceKey: 'CustomerID' });
    models.EyeCareProviders.belongsTo(customer, {foreignKey: 'CustomerID', targetKey: 'CustomerID' , onDelete: 'SET NULL'  });
    customer.hasMany(models.Devices, {foreignKey: 'CustomerID', sourceKey: 'CustomerID' });
    models.Devices.belongsTo(customer, {foreignKey: 'CustomerID', targetKey: 'CustomerID' , onDelete: 'SET NULL'  });
  };
  return customer;
};



