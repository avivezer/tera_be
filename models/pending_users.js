'use strict';
module.exports = (sequelize, DataTypes) => {
    const PendingUser = sequelize.define('PendingUsers', {

        SiteName: {
            type : DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        SitePhoneNumber: {
            unique: true,
            allowNull: true,
            type : DataTypes.STRING,
        },
        SiteEmail: {
            unique: true,
            allowNull: false,
            type : DataTypes.STRING,
        },
        State: DataTypes.STRING,
        Country: {
            unique: false,
            allowNull: false,
            type : DataTypes.STRING,
        },
        City: DataTypes.STRING,
        Street: DataTypes.STRING,
        Apartment: DataTypes.STRING,
        FirstName: DataTypes.STRING,
        LastName: DataTypes.STRING,
        Email: {
            type: DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        PhoneNumber: {
            unique: true,
            allowNull: false,
            type: DataTypes.STRING,
        },
        // Site may have a few admins
        SiteAdmin:{
            type : DataTypes.BOOLEAN,
            allowNull: false,
            defaultValue: true
        },

        DefaultVAUnit:DataTypes.STRING,

        MedicalLicense    : {
            type : DataTypes.STRING,
            allowNull: true,
            unique: true
        },

        Profession  : DataTypes.INTEGER,  //// 1-MD, 2-Optometrist , 3-Orthoptist

    }, {});

    PendingUser.associate = function(models) {

    };
    return PendingUser;
};



