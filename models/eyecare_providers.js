'use strict';
module.exports = (sequelize, DataTypes) => {
  const EyeCareProvider = sequelize.define('EyeCareProviders', {

    //surrogate primary key
    UserIDpk: {
      type : DataTypes.STRING,
      primaryKey: true,
    },

    UserID: {
      type : DataTypes.STRING,
      allowNull: true,
      unique: true
    },

    SiteID: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    // Site may have a few admins
    SiteAdmin:{
      type : DataTypes.BOOLEAN,
      allowNull: false
    },

    // Site may have a few admins
    CustomerAdmin:{
      type : DataTypes.BOOLEAN,
      allowNull: true
    },

    // CustomerID: {
    //   type : DataTypes.STRING,
    //   allowNull: true,
    // },


    // DefaultVAUnit:DataTypes.STRING,

    // MedicalLicense    : {
    //   type : DataTypes.STRING,
    //   allowNull: true,
    //   unique: true
    // },

    //Profession  : DataTypes.INTEGER,  //// 1-MD, 2-Optometrist , 3-Orthoptist
    //Notes :DataTypes.STRING,

}, {
    indexes:[
      {
        unique: false,
        fields:['SiteID']
      }
    ]
  });

  EyeCareProvider.associate = function(models) {

    // EyeCareProvider pointing -> to the PII
    models.PII.hasOne(EyeCareProvider, {foreignKey: 'UserID', sourceKey: 'UserID' ,});
    EyeCareProvider.belongsTo(models.PII, {foreignKey: 'UserID', targetKey: 'UserID' , onDelete: 'SET NULL' , });

//// https://github.com/sequelize/sequelize/issues/4868
    ///https://stackoverflow.com/questions/37964763/what-does-separate-in-sequelize-mean
    EyeCareProvider.hasMany(models.Patients, {foreignKey: 'DoctorID', sourceKey: 'UserID' });  /// foreignKeyConstraint ?
    models.Patients.belongsTo(EyeCareProvider, {foreignKey: 'DoctorID', targetKey: 'UserID'  , onDelete: 'SET NULL' ,});

  };
  return EyeCareProvider;
};



