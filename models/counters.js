'use strict';
const Sequelize = require('sequelize');


module.exports = (sequelize, DataTypes) => {
  const Counters = sequelize.define('Counters', {

    // SiteID
    // UserID  (AdminID XX ,ECPID , PatientID)

    //If you don't define a primaryKey then sequelize uses id by default.


    Name: {
      type : DataTypes.STRING,
      allowNull: false,
      primaryKey: true

    },
    Curr: {
      type: DataTypes.INTEGER,  // ///     // allowNull defaults to true
      defaultValue: 0

    },



    /// end fields
    // options
  }, {

    ////sing `unique: true` in an attribute above is exactly the same as creating the index in the model's options
    // indexes:[
    //   {
    //     unique: true,
    //     fields:['Name']
    //   }
    // ]

  });

  Counters.associate = function(models) {
    // associations can be defined here
  };



  Counters.getCount = function (user) {
    // 'this' refers directly back to the model (the capital Counters)
    let that = this
    return  sequelize.transaction({
      // autocommit : true,
      isolationLevel: Sequelize.Transaction.ISOLATION_LEVELS.READ_UNCOMMITTED
    }, async (t) => {

      /// pay attention to the function option signature
      let effectedRows = await that.update({Curr: sequelize.literal(`Curr + 1`) }, { where: { Name: user }},
          {transaction: t  ,  lock: t.LOCK.UPDATE });

      let ans = await that.findOne({where: {Name: user} , transaction: t ,  lock: t.LOCK.UPDATE  });

      if (effectedRows && Array.isArray(effectedRows) && effectedRows[0] ===1 && ans && ans.Curr>=0 ){
        ans = ans.toJSON()
         // affected row submitted successfully but not committed to find yet
        return  ans.Name + ans.Curr.toString().padStart(2, '0')
      }
      else
        throw new Error("error updating counter for " + user);

    });

  };




  /// end define
  return Counters;
};



