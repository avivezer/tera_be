'use strict';
module.exports = (sequelize, DataTypes) => {
  const site = sequelize.define('Sites', {

    SiteID: {
      type : DataTypes.STRING,
      allowNull: false,
      primaryKey: true   /// can also be referenced, references: {       model: Bar, key: 'id',}
    },

    /// no site admin , only eyeswift admin can control them

    SiteName   : {
      type : DataTypes.STRING,
      allowNull: false,
      unique: true
    },

    CustomerID: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    SiteEmail: {
      type: DataTypes.STRING,
      allowNull: false
    },

    SitePhoneNumber  : {
      type: DataTypes.STRING,
      allowNull: false
    },

    SiteAddress  : {
      type: DataTypes.STRING,
      allowNull: false
    },
    // SiteCountry : DataTypes.STRING,
    // SiteState : DataTypes.STRING,
    // SiteCity: DataTypes.STRING,
    // SiteStreet :DataTypes.STRING,
    // SiteApartment :DataTypes.STRING,
    // SiteZipCode :DataTypes.STRING,

    Enabled :{   /// 0-Lock/on hold, 1-Active
      type : DataTypes.INTEGER,
      allowNull: false,
      defaultValue:1 // active
    },

  }, {
    indexes:[
      {
        unique: false,
        fields:['CustomerID']
      }
    ]
  });

  site.associate = function(models) {
    //BelongsTo will add the foreignKey on the source where hasOne will add on the target
    // so the foreign key is in EyeCareProviders , which means EyeCareProviders pointing -> to the Sites , Sites is the primary
    site.hasMany(models.EyeCareProviders, {foreignKey: 'SiteID', sourceKey: 'SiteID'});
    //sourceKey  referring to a column of the current model ,
    // hasOne association keep the foreign key on the target model
    models.EyeCareProviders.belongsTo(site, {foreignKey: 'SiteID', targetKey: 'SiteID' , onDelete: 'SET NULL'  });
    // targetKey  referring to a column of the target model
    // This creates a foreign key called `SiteID` in the source model (EyeCareProviders)
    // which references the targetKey `SiteID` field from the target model (site).
    site.hasMany(models.Devices, {foreignKey: 'SiteID', sourceKey: 'SiteID' });
    models.Devices.belongsTo(site, {foreignKey: 'SiteID', targetKey: 'SiteID' , onDelete: 'SET NULL'  });
    
  };
  return site;
};



