'use strict';
module.exports = (sequelize, DataTypes) => {
  const Treatments = sequelize.define('Treatments', {

    TreatmentID: {  // SessionID
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },

    UserID:{ // user id of PATIENT
      type : DataTypes.STRING,
      allowNull: true,
    },

    StartTreatment : DataTypes.DATE,
    EndTreatment: DataTypes.DATE,
    TreatmentDuration: DataTypes.FLOAT, // seconds


  }, {
    indexes:[
      {
        unique: false,
        fields:['UserID']
      }
    ]

  });

  // //@see: https://github.com/sequelize/sequelize/issues/1026#issuecomment-54877327
  // Treatments.removeAttribute('id');


  Treatments.associate = function(models) {
    // associations can be defined here
  };
  return Treatments;
};



