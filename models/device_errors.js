'use strict';
module.exports = (sequelize, DataTypes) => {
    
    const DeviceErrors = sequelize.define('DeviceErrors', {

        ErrorID: {
            type : DataTypes.INTEGER,
            primaryKey: true,
            autoIncrement: true,
        },
        DeviceID: {
            type : DataTypes.STRING,
            allowNull: false,
        },
        Error :DataTypes.STRING,
        ErrorDate: DataTypes.DATE

    }, {
        // indexes:[
        //
        // ]

    });


    DeviceErrors.associate = function(models) {
        // associations can be defined here
    };
    return DeviceErrors;
};





