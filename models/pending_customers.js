'use strict';
module.exports = (sequelize, DataTypes) => {
    const PendingCustomer = sequelize.define('PendingCustomers', {

        CustomerName: {
            type : DataTypes.STRING,
            unique: true,
            allowNull: false
        },
        CRMLink: {
            type : DataTypes.STRING,
            allowNull: false
        },
        // Site may have a few admins
        // SiteAdmin:{
        //     type : DataTypes.BOOLEAN,
        //     allowNull: false,
        //     defaultValue: true
        // },

        // DefaultVAUnit:DataTypes.STRING,

        
    }, {});

    PendingCustomer.associate = function(models) {

    };
    return PendingCustomer;
};



