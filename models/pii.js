'use strict';
module.exports = (sequelize, DataTypes) => {
  const PII = sequelize.define('PII', {

    UserID: {
      type : DataTypes.STRING,
      primaryKey:true
    },

    FirstName: {
      type : DataTypes.STRING,
      allowNull: false,
    },

    LastName:{
      type : DataTypes.STRING,
      allowNull: false,
    },

    Email: {
      type: DataTypes.STRING,
      unique: true,
      allowNull: false
    },

    Gender: DataTypes.INTEGER,  /// 1-Male, 2-Female, 3-Other

    Birthdate: DataTypes.STRING,

    SocialID: {
      type : DataTypes.STRING,
      allowNull: true,
      unique: true
    },
    Country: DataTypes.STRING,
    State: DataTypes.STRING,
    City: DataTypes.STRING,
    Street: DataTypes.STRING,
    Apartment: DataTypes.STRING,
    ZIPCode: DataTypes.STRING,

    PhoneNumber: DataTypes.STRING, // required for admin/doctor , but not patients

  }, {uniqueKeys: {
      PII_unique_compound_Email_FirstName_LastName: { /// Index Keyname Importance is for humans only, so don't worry
        fields: ['Email', 'FirstName', 'LastName' ]
      },
      Patient_unique_compound_Birthdate_FirstName_LastName: { /// Index Keyname Importance is for humans only, so don't worry
        fields: ['Birthdate', 'FirstName', 'LastName' ]
      }
    }});

  PII.associate = function(models) {
    // associations can be defined here

    // order matters
    /// pii need to create firsts

    //BelongsTo will add the foreignKey on the source where hasOne will add on the target
    // so the foreign key is in users , which means users pointing -> to the pii , pii is the primary
    PII.hasOne(models.Users, {foreignKey: 'UserID', sourceKey: 'UserID' , onDelete: 'CASCADE' }); //  onDelete: 'set null' - if foreignKey is not primary (primary can be set to null)
    //sourceKey  referring to a column of the current model ,
    // hasOne association keep the foreign key on the target model
    models.Users.belongsTo(PII, {foreignKey: 'UserID', targetKey: 'UserID'  , onDelete: 'CASCADE' , });
    // targetKey  referring to a column of the target model
    ////// This creates a foreign key called `UserID` in the source model (Users)
    // // which references the targetKey `UserID` field from the target model (PII).
  };
  return PII;
};



///sequelize model:create --name PII --attributes GeneralID:Integer,PatientID:String,FirstName:String,LastName:String,Gender:Integer,Birthdate:String,Country:String,State:String,City:String,Street:String,Apartment:String,ZIPCode:String,EmailAddress1:String,EmailAddress2:String,PhoneNumber:String,PhoneNumber2:String
