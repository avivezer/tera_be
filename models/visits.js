'use strict';

const moment = require('moment');


module.exports = (sequelize, DataTypes) => {
  const Visits = sequelize.define('Visits', {

    VisitID: {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },


    UserID:{ // user id of PATIENT
      type : DataTypes.STRING,
      allowNull: false,
    },

    DoctorID:{ // user id of assigned doctor
      type : DataTypes.STRING,
      allowNull: true,
    },

    VisitDate: {
      type : DataTypes.DATE(6),
      allowNull: false,
    },

    AmblyopicEye:  {
      type : DataTypes.INTEGER, // fixed , 1 = left , 2=right // device need last
      allowNull: false,
    },

    NearVAUnit: DataTypes.INTEGER,              //  fixed, 1-logmar, 2-foot, 3-meter , 4-decimal // saved as logmar , frontend will convert it according to he's needs
    DistanceVAUnit: DataTypes.INTEGER,          //  fixed, 1-logmar, 2-foot, 3-meter , 4-decimal // saved as logmar

    RightEyeNearVA:{                 // device need last record
      allowNull: true,
      type: DataTypes.FLOAT
    },
    LeftEyeNearVA: {                 // device need last record
      allowNull: true,
      type: DataTypes.FLOAT
    },

    VAPrescription: DataTypes.INTEGER,

    RightEyeDistanceVA: {                 // device need last record
      allowNull: false,
      type: DataTypes.FLOAT
    },

    LeftEyeDistanceVA: {                 // device need last record
      allowNull: false,
      type: DataTypes.FLOAT
    },


    BinocularEyeNearVA: DataTypes.FLOAT,
    BinocularEyeFarVA: DataTypes.FLOAT,
    StereoAcuityBookType: DataTypes.INTEGER,
    StereoTestType: DataTypes.INTEGER,
    StereoValue: DataTypes.FLOAT,

    WeeklyNumberOfSessions: {                 // device need last record
      allowNull: false,
      type: DataTypes.INTEGER
    },
    RecommendedSessionTime: {                 // device need last record
      allowNull: false,
      type: DataTypes.FLOAT
    },
    TreatmentDuration:{                  // device need last record
      allowNull: false,
      type: DataTypes.INTEGER
    },

    CompatibilityTest: DataTypes.INTEGER,
    NearDeviationHorizontalDirection: DataTypes.INTEGER,
    NearDeviationHorizontalSuffixes: DataTypes.INTEGER,
    NearDeviationHorizontalAmplitude: DataTypes.FLOAT,
    DistanceDeviationHorizontalDirection: DataTypes.INTEGER,
    DistanceDeviationHorizontalSuffixes: DataTypes.INTEGER,
    DistanceDeviationHorizontalAmplitude: DataTypes.FLOAT,
    DistanceDeviationVerticalAmplitude: DataTypes.FLOAT,
    DistanceDeviationVerticalDirection: DataTypes.INTEGER,
    DistanceDeviationVerticalSuffixes: DataTypes.INTEGER,
    NearDeviationVerticalAmplitude:DataTypes.FLOAT,
    NearDeviationVerticalDirection: DataTypes.INTEGER,
    NearDeviationVerticalSuffixes:DataTypes.INTEGER,
    RefractionSphereLeft: DataTypes.FLOAT,
    RefractionCylinderLeft: DataTypes.FLOAT,
    RefractionAxisLeft: DataTypes.INTEGER,
    RefractionADDLeft: DataTypes.FLOAT,
    RefractionPrismLeft: DataTypes.FLOAT,
    RefractionBaseLeft: DataTypes.INTEGER,  /// 0-Up, 1-Down, 2-In, 3-Out
    RefractionSphereRight: DataTypes.FLOAT,
    RefractionCylinderRight: DataTypes.FLOAT,
    RefractionAxisRight: DataTypes.INTEGER,
    RefractionADDRight: DataTypes.FLOAT,
    RefractionPrismRight: DataTypes.FLOAT,
    RefractionBaseRight: DataTypes.INTEGER,// 0-Up, 1-Down, 2-In, 3-Out
    PrescriptionDate: DataTypes.DATE,
    PrescriptionType: DataTypes.INTEGER,  /// 012 Distance, near, intermediate
    PrescriptionSphereLeft: DataTypes.FLOAT,
    PrescriptionCylinderLeft: DataTypes.FLOAT,
    PrescriptionAxisLeft: DataTypes.INTEGER,
    PrescriptionADDLeft: DataTypes.FLOAT,
    PrescriptionPrismLeft: DataTypes.FLOAT,
    PrescriptionBaseLeft: DataTypes.INTEGER, //0-Up, 1-Down, 2-In, 3-Out
    PrescriptionSphereRight: DataTypes.FLOAT,
    PrescriptionCylinderRight: DataTypes.FLOAT,
    PrescriptionAxisRight: DataTypes.INTEGER,
    PrescriptionADDRight: DataTypes.FLOAT,
    PrescriptionPrismRight: DataTypes.FLOAT,
    PrescriptionBaseRight: DataTypes.INTEGER,  //0-Up, 1-Down, 2-In, 3-Out
    TreatmentPrescriptionDate: DataTypes.DATE,
    TreatmentPrescriptionType: DataTypes.INTEGER,  ///// 012 Distance, near, intermediate
    TreatmentPrescriptionSphereLeft: DataTypes.FLOAT,
    TreatmentPrescriptionCylinderLeft: DataTypes.FLOAT,
    TreatmentPrescriptionAxisLeft: DataTypes.INTEGER,
    TreatmentPrescriptionADDLeft: DataTypes.FLOAT,
    TreatmentPrescriptionPrismLeft: DataTypes.FLOAT,
    TreatmentPrescriptionBaseLeft: DataTypes.INTEGER,  //// 0-Up, 1-Down, 2-In, 3-Out
    TreatmentPrescriptionSphereRight: DataTypes.FLOAT,
    TreatmentPrescriptionCylinderRight: DataTypes.FLOAT,
    TreatmentPrescriptionAxisRight: DataTypes.INTEGER,
    TreatmentPrescriptionADDRight: DataTypes.FLOAT,
    TreatmentPrescriptionPrismRight: DataTypes.FLOAT,
    TreatmentPrescriptionBaseRight: DataTypes.INTEGER,

    TreatmentHistory: DataTypes.INTEGER,   /// 0-None, 1-Patch, 2-Atropine, 3-Other.
    TreatmentHistoryNotes: DataTypes.STRING,
    AdvanceNotes: DataTypes.STRING


  }, {
    indexes:[
      {
        unique: false,
        fields:['UserID']
      },
      {
        unique: false,
        fields:['VisitDate']
      },
      {
        unique: false,
        fields:[ 'UserID', 'VisitDate'] // (in that order) , // filtering and sorting
        // MySQL should show REF access method over this index in EXPLAIN EXTENDED.
      }
    ]
  });

  Visits.associate = function(models) {
    // associations can be defined here
  };
  return Visits;
};


///sequelize model:create --name Visits --attributes PatientID:String,VisitID:Integer,VisitDate:Date,AmblyopicEye:Integer,VAprescription:Integer,NearVAUnit:Integer,DistanceVAUnit:Integer,RightEyeNearVA:Float,LeftEyeNearVA:Float,RightEyeDistanceVA:Float,LeftEyeDistanceVA:Float,BinocularEyeNearVA:Float,BinocularEyeFarVA:Float,StereoAcuityBookType:Integer,StereoTestType:Integer,StereoValue:Float,WeeklyNumberofSessions:Integer,RecommendedSessionTime:Float,TreatmentDuration:Integer,CompatibilityTest:Integer,NearDeviationHorizontalDirection:Integer,NearDeviationHorizontalsuffixes:Integer,NearDeviationhorizontalAmplitude:Float,DistanceDeviationHorizontalDirection:Integer,DistanceDeviationHorizontalsuffixes:Integer,DistanceDeviationhorizontalAmplitude:Float,RefractionSphereLeft:Float,RefractionCylinderLeft:Float,RefractionAxisLeft:Integer,RefractionADDleft:Float,RefractionPrismleft:Float,RefractionBaseleft:Integer,RefractionSphereright:Float,RefractionCylinderright:Float,RefractionAxisright:Integer,RefractionADDright:Float,RefractionPrismright:Float,RefractionBaseright:Integer,PrescriptionDate:Date,PrescriptionType:Integer,PrescriptionSphereLeft:Float,PrescriptionCylinderLeft:Float,PrescriptionAxisLeft:Integer,PrescriptionADDleft:Float,PrescriptionPrismleft:Float,PrescriptionBaseleft:Integer,PrescriptionSphereright:Float,PrescriptionCylinderright:Float,PrescriptionAxisright:Integer,PrescriptionADDright:Float,PrescriptionPrismright:Float,PrescriptionBaseright:Integer,TreatmentPrescriptionDate:Date,TreatmentPrescriptionType:Integer,TreatmentPrescriptionSphereLeft:Float,TreatmentPrescriptionCylinderLeft:Float,TreatmentPrescriptionAxisLeft:Integer,TreatmentPrescriptionADDleft:Float,TreatmentPrescriptionPrismleft:Float,TreatmentPrescriptionBaseleft:Integer,TreatmentPrescriptionSphereright:Float,TreatmentPrescriptionCylinderright:Float,TreatmentPrescriptionAxisright:Integer,TreatmentPrescriptionADDright:Float,TreatmentPrescriptionPrismright:Float,TreatmentPrescriptionBaseright:Integer,Treatmenthistory:Integer,Treatmenthistorynote:String,AdvanceNotes:String
