'use strict';
module.exports = (sequelize, DataTypes) => {
  const SystemVersions = sequelize.define('SystemVersions', {

    SystemVersionID:  {
      type: DataTypes.INTEGER,
      autoIncrement: true,
      primaryKey: true
    },

    VersionType: DataTypes.STRING,     // WIN, CSAPP, ET
    VersionNO:   DataTypes.STRING,
    VersionURL:  DataTypes.STRING,

    AdminID: {
      allowNull:true,
      type: DataTypes.STRING
    },

}, {uniqueKeys: {
      Version_unique_compound_VersionType_VersionNO: { /// Index Keyname Importance is for humans only, so don't worry
        fields: ['VersionType', 'VersionNO' ]
      },
    }});


  SystemVersions.gelLatestVersion =async function() {

    // let vers =await this.findAll({
    //   attributes: [ 'SystemVersionID',
    //     [sequelize.fn("max", sequelize.col('createdAt')), 'latestVersion']
    //   ],
    //   group: ['VersionType']
    // });

    ///We can't reference the result of an aggregate function (for example MAX()

    const results = await sequelize.query(

        "SELECT * " +
        "FROM SystemVersions sv JOIN" +
        " ( SELECT MAX(sv2.VersionNO) AS max_ver FROM SystemVersions sv2 GROUP BY VersionType) m " + // group by is a column
        " ON m.max_ver = sv.VersionNO"
        , { type: sequelize.QueryTypes.SELECT });  //  don't need to access the metadata (results WONT BE ARRAY OF 2 RESULTS)

    let object = results.reduce((obj, item) => (obj[item.VersionType] = item, obj) ,{});

    if (!object || Object.keys(object).length === 0)
      return null

    return object
  };

    SystemVersions.associate = function(models) {

    models.PII.hasOne(SystemVersions, {foreignKey: 'AdminID', sourceKey: 'UserID' });
    SystemVersions.belongsTo(models.PII, {foreignKey: 'AdminID', targetKey: 'UserID'  , onDelete: 'CASCADE' , });

  };




  return SystemVersions;
};



