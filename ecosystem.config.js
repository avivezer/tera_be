// pmt stop all
// pm2 monit              # Monitor all processes
//pm2 start

module.exports = {
  apps : [{
    name: 'j API',
    script: 'app.js',

    // Options reference: https://pm2.io/doc/en/runtime/reference/ecosystem-file/
    // args: 'one two',
    instances: "2",   // "max" not good
    exec_mode  : "cluster",
    autorestart: true,
    watch: false,
    max_memory_restart: '1G', //your app will be restarted if it exceeds the amount of memory
    instance_var: "INSTANCE_ID",
    log: null, //'./combined.outerr.log'
    merge_logs: true,
    env: {
      NODE_ENV: 'development'
    },
    env_production: {
      NODE_ENV: 'production'
    }
  }],

  deploy : {
    production : {
      user : 'node',
      host : '212.83.163.1',
      ref  : 'origin/master',
      repo : 'git@github.com:repo.git',
      path : '/var/www/production',
      'post-deploy' : 'npm install && pm2 reload ecosystem.config.js --env production'
    }
  }
};
