const expressWinston = require('express-winston');
const winston = require('winston');
require('winston-daily-rotate-file'); //override winston.transports.file
const httpContext = require('express-http-context');

expressWinston.requestWhitelist.push('body'); ////include req.body
expressWinston.requestWhitelist.push('ip');  //include req.ip
expressWinston.requestWhitelist.push('reqId');  //include req.ip
expressWinston.responseWhitelist.push('body');

//https://github.com/bithavoc/express-winston

// {  // each level include also the lower value (info is 6 ,5,4,3,2,1.0)
//     error: 0,
//     warn: 1,
//     info: 2,
//     verbose: 3,
//     debug: 4,
//     silly: 5
// }

const MESSAGE = Symbol.for('message');
const ReqIDFormatter = (logEntry) => {
    // logEntry[MESSAGE] = logEntry[MESSAGE] + newf
    let reqId = httpContext.get('reqId');
    logEntry["reqId"] = reqId
    return logEntry;
}

// Ignore specific log messages
const ignorePrivate = winston.format((info, opts) => {
    if (info.meta && info.meta.error && info.meta.error.message) {

        if (info.meta.error.message === 'Wrong UserID or Password' ||
            info.meta.error.message === 'unauthorized'||
            info.meta.error.message.startsWith('bad UserID')||
            info.meta.error.message.startsWith('joi') ||
            info.meta.error.message.startsWith('JsonWebTokenError') ||

            info.meta.error.message.startsWith('Validation error')    // sequlize - 1 sec latency to print stack !
        )
            return false;

        if (info.meta.error.message === 'Validation failed') //mongoose
            return false;
    }
    return info;
});

const reqResLogger = expressWinston.logger({
    transports: [
        new winston.transports.Console({eol: '\r\n\r\n' }),
        // new winston.transports.DailyRotateFile({ filename: 'logs/%DATE%-info.log' , eol: '\r\n\r\n'}) ,
        // new winston.transports.DailyRotateFile({ filename: 'logs/%DATE%-error.log' , eol: '\r\n\r\n' , level:'warn'}) //copy again only errors
    ],
    level: function (req, res) {
        let level = "";
        if (res.statusCode >= 100) { level = "info"; }
        if (res.statusCode >= 400) { level = "error"; }
        return level;
    },
    statusLevels: false ,

    format: winston.format.combine(
        ignorePrivate(),
        winston.format(ReqIDFormatter)(),
        winston.format.timestamp(),
        winston.format.simple() // / (can ignore routes) winston.format.prettyPrint() //
    ),

    //meta: Boolean, // control whether you want to log the meta data about the request (default to true).
    //dynamicMeta: function(req, res) { return [Object]; }, // Extract additional meta data from request or response (typically req.user data if using passport). meta must be true for this function to be activated

    ignoreRoute: function (req, res) {
        if ( req.path.toString().startsWith('/api/'))
            return false
        return true; },
});



const reqResErrorLogger = expressWinston.errorLogger({
    level: 'error',
    winstonInstance :winston.createLogger({
        exitOnError:false , // workaround
        transports: [
            new winston.transports.Console({eol: '\r\n\r\n' ,level: 'error' , handleExceptions: true }), /// bug winston when  handleExceptions: true process exit
            // new winston.transports.DailyRotateFile({ filename: 'logs/%DATE%-error.log',
            //     level: 'error' ,
            //     eol: '\r\n\r\n' ,
            //     handleExceptions: true
            // }),
        ],
        format: winston.format.combine(
            ignorePrivate(),
            winston.format(ReqIDFormatter)(),
            winston.format.timestamp(),
            winston.format.prettyPrint(),
            // winston.format.printf(info => `${info.level}` ),
        )

    }),
    // meta: true,
    // blacklistedMetaFields: [ "stack" , "errors" , "trace"],

});


const Logger = winston.createLogger({
    exitOnError:false,
    level: 'info',
    format: winston.format.combine(
        winston.format(ReqIDFormatter)(),
        winston.format.timestamp(),
        winston.format.simple()

    ),
    transports: [
        new winston.transports.Console(),
        // new winston.transports.DailyRotateFile({ filename: 'logs/%DATE%-info.log', level: 'info' }),

    ]
});

//transport console to logger
//https://github.com/winstonjs/winston/issues/1427
console.log = (...args) => Logger.info(...args);
console.info = (...args) => Logger.info(...args);
console.warn = (...args) => Logger.warn(...args);
console.error = (...args) => Logger.error(...args);
console.debug = (...args) => Logger.debug(...args);

// module.exports = {reqResLogger, reqResErrorLogger , Logger }

module.exports = {Logger ,reqResLogger ,reqResErrorLogger }
