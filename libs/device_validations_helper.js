const joi = require('@hapi/joi');

module.exports = {

    DeviceValidateBody: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req.body);
            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['body'] = result.value;
            next();
        };
    },

    DeviceValidateQuery: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req["query"]);

            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['query'] = result.value;
            next();
        };
    },

    ValidateHeader: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req.headers);

            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['headers'] = result.value;
            next();
        };
    },

    DeviceSchemas: {

        //everything else that needed and not in user management
        deviceSignupSchema: joi.object().keys({
            SerialNumber:joi.string().required(),
            DeviceSig: joi.string().allow(null),
            HardwareVersion:joi.string().allow(null),
            FirmwareVersion:joi.string().allow(null),
            IsEnabled:joi.boolean(),
            Notes:joi.string().allow(null),
        }).options({stripUnknown: true , abortEarly:false}),


        deviceUpdateSchema: joi.object().keys({
            // SerialNumber:joi.string(),
            HardwareVersion:joi.string().allow(null),
            FirmwareVersion:joi.string().allow(null),
            DeviceSig: joi.string().allow(null),
            IsEnabled:joi.boolean(),
            Notes:joi.string().allow(null),

        }).options({stripUnknown: true, abortEarly:false}),


        systemStatusSchema: joi.array().items(
            joi.object().keys({
                ErrorDate: joi.date().iso().required(),
                Error: joi.string().required()
            }).options({stripUnknown: true, abortEarly:false})
        ).min(1).required(),

        deviceTreatmentSchema: joi.array().items(
            joi.object().keys({
                StartTreatment: joi.date().iso().required(),
                EndTreatment: joi.date().iso().required(),
            }).options({stripUnknown: true, abortEarly:false}),
        ).min(1).required(),


        PatientForgotSchema: joi.object().keys({
            PatientID:joi.string().required(),
        }).options({stripUnknown: true, abortEarly:false}),

    },

    HeadersSchemas: joi.object().keys({
        SerialNumber:joi.string().required(),
    }).options({
        //remove unknown field from the validated body
        stripUnknown: true,
        // return all errors a payload contains, not just the first one Joi finds
        abortEarly: false
    }).rename("serialnumber" ,"SerialNumber"),



};
