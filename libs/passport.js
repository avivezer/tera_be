const ENV_CONFIG = require('../configurations/env_config');
const passport = require('passport');
const JwtStrategy = require('passport-jwt').Strategy;
const LocalStrategy = require('passport-local').Strategy;
const { ExtractJwt } = require('passport-jwt');
// const Promise = require('bluebird');
const models  = require('../models'); // this is how you access all sequelize models


//fixme arrange to same logic structure in each strategy , and return 422 instead of error


// LOCAL Web STRATEGY
passport.use('allWebUsersSignin' , new LocalStrategy ({
        usernameField: 'Email',
        passwordField: 'Password',
        passReqToCallback:true
    },
    //done is the authenticate callback
  async (req, username, password, done) => {
    try {
        username =  req.value.userinfo.Email
        password =  req.value.userinfo.Password
      // Find the user given the pid
        const Piiuser  = await models.PII.findOne({where:{ Email: username }} );
        let user;
        if (Piiuser)
             user  = await models.Users.scope("withAll").findOne({ where:{ UserID: Piiuser.UserID }}  );

      // If not, handle it
      if (!user || !Piiuser) {
        return done(null, false);
      }
        if (! user.Enabled ) {
            return done(null, false, "User disabled (locked)");
        }
      // Check if the password is correct
      if (! await user.checkPassword(password)) {
          if (user.FailedLoginAttempts >= 5 && !user.Role.includes('EyeSwiftSuperAdmin'))
              await user.update({FailedLoginAttempts: models.sequelize.literal(`FailedLoginAttempts + 1`) , Enabled : 0   });
          else
            await user.increment ('FailedLoginAttempts');

          return done(null, false); // error pass incorrect
      }
        //ok
        let LastLoginAt = new Date()
        await user.update({'FailedLoginAttempts' : 0 , LastLoginAt:LastLoginAt, TwoFaCode:null })

      // If this function gets here , done is with true, authentication was successful.
      //     // `req.user` contains the authenticated user.
      const fullUser = Object.assign(user.toJSON() , Piiuser.toJSON() ,
        {     Password:undefined ,
              PassNeedsToChange: user.PassNeedsToChangeAt > 0 ? user.PassNeedsToChangeAt < LastLoginAt : false,
              PassNeedsToChangeAt:undefined,
              forgotAt:undefined,FailedLoginAttempts:undefined , LastLoginAt:undefined
          })
      done(null, fullUser);

    } catch (error) {
      done(error, false);
    }
  }));


// LOCAL Device STRATEGY
passport.use('allDeviceUsersSignin' , new LocalStrategy ({
        usernameField: 'Email',
        passwordField: 'Password',
        passReqToCallback:true

    },
    //done is the authenticate callback
    async (req, username, password, done) => {
        try {
            // Find the user given the pid

            const piiUser  = await models.PII.findOne({ where:{ Email: username }}  );
            let user
            let doctorUser
            if (piiUser)
                user  = await models.Users.scope("withAll").findOne({ where:{ UserID: piiUser.UserID }}  );
            if (user)
                doctorUser  = await models.EyeCareProviders.findOne({where:{ UserID: user.UserID }} );

            // If not, handle it
            if (!piiUser || !user || !doctorUser) {
                return done(null, false);
            }
            if (! user.Enabled ) {
                return done(null, false, "User disabled (locked)");
            }
            // Check if the password is correct
            if (! await user.checkPassword(password)) {
                if (user.FailedLoginAttempts >= 5 )
                    await user.update({FailedLoginAttempts: models.sequelize.literal(`FailedLoginAttempts + 1`) , Enabled : 0   });
                else
                    await user.increment ('FailedLoginAttempts');

                return done(null, false);
            }
            //ok
            let LastLoginAt = new Date()
            await user.update({'FailedLoginAttempts' : 0 , LastLoginAt:LastLoginAt })
            // If this function gets called, authentication was successful.
            //     // `req.user` contains the authenticated user.
            const fullUser = Object.assign(user.toJSON(), piiUser.toJSON(),
                {EyeCareProvider: doctorUser.toJSON()},
                {Password:undefined,PassNeedsToChangeAt:undefined,
                 forgotAt:undefined,FailedLoginAttempts:undefined , LastLoginAt:undefined
                })
            done(null, fullUser);

        } catch (error) {
            done(error, false);
        }
    }));

async function PassportLocalAuth(req, res, next) {

    passport.authenticate('allWebUsersSignin', {session: false, failWithError: true}, (err, DBuser, info) => {
            if (err) {
                return next(err);
            }
            if (!DBuser && info) {
                return next(new Error(info));
            }
            if (!DBuser) {
                return next(new Error("Wrong UserID or Password"));
            }
            req.user = DBuser
            return next()
        }
    )(req, res, next)
}

async function PassportDeviceLocalAuth(req, res, next) {

    passport.authenticate('allDeviceUsersSignin', {session: false, failWithError: true}, (err, DBuser, info) => {
            if (err) {
                return next(err);
            }
            if (!DBuser && info) {
                return next(new Error(info));
            }
            if (!DBuser) {
                return next(new Error("Wrong UserID or Password"));
            }
            req.user = DBuser
            return next()
        }
    )(req, res, next)
}

// JSON WEB TOKENS STRATEGY , called first and decoded the token
passport.use('jwt', new JwtStrategy ({
        secretOrKey: ENV_CONFIG.ENV_JWT_SECRET,
        jwtFromRequest: ExtractJwt.fromHeader('authorization'),
        issuer : 'EyeSwift',
        ignoreExpiration:false,  // if to ignore exp validation
        passReqToCallback:true
    },
    //done is the authenticate callback, it reach here if jwt.verify pass successfully
    async (req ,payload, done) => {
        try {
            //no need to hit database for user here
            done(null,  payload);  // token decoded payload
        } catch(error) {
            done(error, false);
        }
    }));

// JSON WEB TOKENS STRATEGY , called first and decoded the token
passport.use('jwt2fa', new JwtStrategy ({
        secretOrKey: ENV_CONFIG.ENV_JWT_SECRET+1,
        jwtFromRequest: ExtractJwt.fromHeader('authorization'),
        issuer : 'EyeSwift',
        ignoreExpiration:false,  // if to ignore exp validation
        passReqToCallback:true,
    },
    //done is the authenticate callback, it reach here if jwt.verify pass successfully
    async (req ,payload, done) => {
        try {
            //no need to hit database for user here
            done(null,  payload);  // token decoded payload
        } catch(error) {
            done(error, false);
        }

    }));


// called second
async function PassportJWTAuth(req, res, next) {
    passport.authenticate('jwt', {session: false, failWithError: true}, async (err, token, info) => {  //info is
        if (err) {
            return next(err);
        }
        if (!token) {
            return next(new Error(info));
        }
        req.userToken = token
        try {
            // Optional - Find the user given the pid
            const user = await models.Users.findOne({where: {UserID: token.sub}, attributes: {exclude: ['Password']}});
            if (!user) {            // If not, handle it
                return next(new Error("No such user"));
            }
            const Piiuser = await models.PII.findOne({where: {UserID: token.sub}});
            if (!Piiuser) {            // If not, handle it
                return next(new Error("No such user pii"));
            }
            if (user.IsEnabled === false) {
                return next(new Error("User disabled"));
            }

            req.user = Object.assign(user.toJSON(), Piiuser.toJSON())
            return next()
        } catch (e) {
            return next(e)
        }

    })(req, res, next)
}


// called second
async function PassportJWT2FAAuth(req, res, next) {
    passport.authenticate('jwt2fa', {session: false, failWithError: true}, async (err, token, info) => {  //info is
        if (err) {
            return next(err);
        }
        if (!token) {
            return next(new Error(info));
        }
        req.userToken = token // the temp token

        try {

            const user  = await models.Users.scope("withAll").findOne({ where:{ UserID: token.sub }}  );
            if (!user) {            // If not, handle it
                return next(new Error("No such user"));
            }
            if (req.value.userinfo.TwoFaCode !== user.TwoFaCode) {
                if (user.FailedLoginAttempts >= 5 && !user.Role.includes('EyeSwiftSuperAdmin'))
                    await user.update({FailedLoginAttempts: models.sequelize.literal(`FailedLoginAttempts + 1`) , Enabled : 0   });
                else
                    await user.increment ('FailedLoginAttempts');
                return next(new Error("unauthorized code"));
            }

            const Piiuser =await models.PII.findOne({ where:{ UserID: user.UserID }}  );
            if (!Piiuser) {            // If not, handle it
                return next(new Error("No such user pii"));
            }
            if (user.IsEnabled === false) {
                return next(new Error("User disabled"));
            }

            //ok
            let LastLoginAt = user.LastLoginAt // also the ~time that first token issued
            // if (new Date() -  LastLoginAt  >  3600) {
            //     return next(new Error("error last login"));
            // }

            await user.update({'FailedLoginAttempts' : 0 , LastLoginAt:LastLoginAt, TwoFaCode:null })

            req.user = Object.assign(user.toJSON(),
                {
                    PassNeedsToChange: user.PassNeedsToChangeAt > 0 ? user.PassNeedsToChangeAt < LastLoginAt : false,
                    PassNeedsToChangeAt:undefined,
                    Password: undefined, TwoFA: undefined,
                    forgotAt: undefined, FailedLoginAttempts: undefined, LastLoginAt: undefined
                })

            return next()

        }catch (e) {
            next(e)
        }

    })(req, res, next)
}



/**
 * validate user id in jwt against param user scope(the logic func searching the db by using param) ,
 and validates role type permission to this route.
 check  the role in the signed jwt , and that the param(user) matches to token subject
 without hitting in the db
 role is array.
 some=true: at least one role in mustRule is inside tokenrole
 some=false: every role (all) in mustRole should be in token role (mustRule is subset of tokenrole)
 */
async function IdCompare  (req, res, next ,param ,mustRole , some) {
    let func  =  some ? "some" : "every"; // some=true:(at least one) , some=false: every role (all) should matches

    const givenId = req.params[param];
    const tokenId = req.userToken.sub; // the jwt strategy put the user here
    const tokenRole = req.userToken.role; // the jwt strategy put the user here
    if (givenId === tokenId && givenId  && Array.isArray(tokenRole) &&
        tokenRole.length && mustRole[func](r=> tokenRole.includes(r)) )     /// validate that all of the roles inside the token matches the route demands
        next();
    else {
        throw new Error("unauthorized id")
    }
}

/**
 check only the role in the signed jwt
 role is array
 */
async function checkRole(req, res, next , mustRole , some , local) {
    let func  =  some ? "some" : "every"; // some=true:(at least one) , some=false: every role (all) should matches
    // return some = true if in the req.user.role jwt has one of the roles in $role which ok to pass
    const tokenRole = local ? req.user.Role : req.userToken.role; // the jwt strategy put the user token in userToken , the local hit the db and put user in req.user
    if ( Array.isArray(tokenRole) && tokenRole.length && mustRole[func](r=> tokenRole.includes(r)) )  //method "some()" tests whether at least one element in the array passes the lambda
        next();                                                                            /// "every()" method tests whether all elements in the array pass the test
    else {
        throw new Error("unauthorized Role")
    }
}


module.exports = {PassportLocalAuth ,PassportDeviceLocalAuth ,PassportJWTAuth ,PassportJWT2FAAuth , IdCompare  , checkRole};
