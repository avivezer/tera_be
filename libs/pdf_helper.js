const ENV_CONFIG = require('../configurations/env_config');
const AWS = require('aws-sdk');
const fs = require('fs')
const moment = require('moment');
const MemoryStream = require('memorystream');
const PdfPrinter = require('pdfmake');

const fonts = {
    Courier: {
        normal: 'Courier',
        bold: 'Courier-Bold',
        italics: 'Courier-Oblique',
        bolditalics: 'Courier-BoldOblique'
    },
    Helvetica: {
        normal: 'Helvetica',
        bold: 'Helvetica-Bold',
        italics: 'Helvetica-Oblique',
        bolditalics: 'Helvetica-BoldOblique'
    },
    Times: {
        normal: 'Times-Roman',
        bold: 'Times-Bold',
        italics: 'Times-Italic',
        bolditalics: 'Times-BoldItalic'
    },
    Symbol: {
        normal: 'Symbol'
    },
    ZapfDingbats: {
        normal: 'ZapfDingbats'
    }
};
const printer = new PdfPrinter(fonts);









// [ {Name:String , Val:{} , _id: false }], //Burning : True
function formatvalues(arr) {
    let ans = []
    for (const [index, element] of arr.entries()){
        if (element.Val === true || (element.Name.toLowerCase().startsWith("other") && element.Val!=="" )  ) {
            ans.push(element.Name);
            ans.push("\n")
        }
        // if(index === arr.length -1)
        //     ans.push("\n")
    }

    return ans
}



function pdfCreate(doctor, test , clinic) {

    // arrs with /n
    let PatientComplaints_format = formatvalues(test.Complaints)
    let PatientEthnicityformat = formatvalues(test.Ethnicity)
    let physicalExaminationformat = formatvalues(test.PhysicalExamination)
    let Physiciandiagnosisformat = formatvalues(test.Diagnosis)

    let Labresultformat = formatvalues(test.LabResult)
    if (Labresultformat.length===0)
        Labresultformat = ["Not yet completed"]

    let body = [

        {
            alignment: 'justify',
            columns: [

                {
                    image: 'assets/logo.png',
                    width: 150
                },
                {
                    text: moment().format("DD MMM YYYY"),
                    alignment: 'right',

                }
            ]
        },
        {
            text: [
                "\n\n\n\n",
                clinic.ClinicName +  "Clinic"+ "\n\n",

                clinic.Location.Country +"\n",
                clinic.Location.City +"\n",
                clinic.Location.Address +"\n\n",

                clinic.ClinicPhone +"\n\n",

                "Dr. " + doctor.LastName + " " + doctor.FirstName +"\n",
                doctor.Email,
            ]
        },
        {
            text: [
                {text:
                    "\n\n"+
                    "Summary"+
                    "\n\n",
                    fontSize: 19, bold: true}
            ]
        },

        {
            alignment: 'justify',
            columns: [
                {
                    width:150,
                    text: [
                        {text: "Test number"}
                    ],
                },
                {
                    width: 'auto',
                    text: [
                        {text: test.TestID.toString(), bold:true}
                    ],
                },
            ]
        },
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "Test time"}
                    ],
                },
                {
                    width: 'auto',
                    text: [
                        {text: moment(test.Start_scan.DateLocal).format('DD MMM YYYY   h:mm')}
                    ],
                },
            ]
        },
        "\n",
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "Patient Complaints"}
                    ],
                },
                {
                    width: 'auto',
                    text: PatientComplaints_format
                },
            ]
        },
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "Patient YOB"}
                    ],
                },
                {
                    width: 'auto',
                    text: [
                        {text: test.PatientYearOfBirth}
                    ],
                },
            ]
        },
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "Patient ethnicity"}
                    ],
                },
                {
                    width: 'auto',
                    text: PatientEthnicityformat
                },
            ]
        },
        "\n",
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "physical Examination",}
                    ],
                },
                {
                    width: 'auto',
                    text: physicalExaminationformat
                },
            ]
        },
        "\n",
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "Physician Diagnosis",}
                    ],
                },
                {
                    width: 'auto',
                    text: Physiciandiagnosisformat
                },
            ]
        },
        "\n",
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "PH level"}
                    ],
                },
                {
                    width: 'auto',
                    text: [
                        {text: test.PatientPhLevel},  // doctor, not algo
                    ],
                },
            ]
        },
        "\n",
        {
            alignment: 'justify',
            columns: [
                {
                    width: 150,
                    text: [
                        {text: "Lab result"}
                    ],
                },
                {
                    width: 'auto',
                    text: Labresultformat
                },
            ]
        },

    ];


    let styles = {
        header: {
            fontSize: 18,
            bold: true
        },
        bigger: {
            fontSize: 15,
            italics: true
        },
        defaultStyle: {
            columnGap: 5,
            lineHeight: 1.3,
            font: 'Helvetica'
            // lineSpacing: {
            //     margin:[0,0,0,2] //change number 6 to increase nspace
            // }
            // lineGap : 1.5
        }
    };


    let docDefinition = {
        info: {
            title: 'Test results',
            author: 'GYNI support',
            subject: 'Doctor'
        },
        footer: { text: 'Copyright \xA92019 GynTools Ltd.', alignment: 'center' },

        content: body,
        styles: styles, // for specific style of content element
        defaultStyle: styles.defaultStyle
    };

    let pdfDoc = printer.createPdfKitDocument(docDefinition);
    return pdfDoc

}


function pdfEnd(doc) {

    let memStream = new MemoryStream(null, {
        readable: false
    });
    // the doc object will output its whole object to the memStream on end.
    doc.pipe(memStream);

    let myEmailPromise = new Promise((resolve, reject) => {

        doc.on('end', async function () {
            // get a blob you can do whatever you like with
            let buffer = Buffer.concat(memStream.queue)
            resolve(buffer);
        });
        // Set up the timeout
        setTimeout(function () {
            reject('Promise rawEmail timed out')
        }, 3000); // 3 second to end creation of the email pdf, if it will resolve in time , reject atfter wont do nothing

        doc.end(); // calling to finish doc

    });

    return myEmailPromise
}




module.exports = {

    pdfCreate,
    pdfEnd,

}

