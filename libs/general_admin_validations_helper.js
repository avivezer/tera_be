const joi = require('@hapi/joi');

module.exports = {

    GeneralValidateBody: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req.body);
            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['body'] = result.value;
            next();
        };
    },


    GeneralValidateQuery: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req["query"]);

            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['query'] = result.value;
            next();
        };
    },



    GeneralSchemas: {

        //todo schemas

        // querySchema: {
        //     query: joi.object({  //this is a get scheme - schema.validate(req[params] this is the :name) or (query - this is all fields(?time={{now}}) on the get url)
        //         UserID: joi.string()
        //     }),
        //     // headers: joi.object({
        //     //     Authorization: joi.string().token().required()
        //     // }).unknown() //unknown allows other headers like Content-Type etc.
        // },

        addVersionsSchema: joi.object().keys({
            VersionType: joi.string().valid("WIN", "ESAPP", "ET", "EMITTER", "TV").required(),     // WIN, CSAPP, ET
            VersionNO:   joi.string().required(),
            VersionURL:  joi.string().required(),
        }).options({stripUnknown: true , abortEarly:false}),

        EditVersionsSchema: joi.object().keys({
            SystemVersionID: joi.number().required(),
            VersionType: joi.string().valid("WIN", "ESAPP", "ET", "EMITTER", "TV").required(),     // WIN, CSAPP, ET
            VersionNO:   joi.string().required(),
            VersionURL:  joi.string().required(),
        }).options({stripUnknown: true , abortEarly:false}),

        sendMessagesDoctorsSchema: joi.object().keys({
            Method :joi.string().valid("SMS" , "EMAIL", "BOTH"),
            DoctorsIDs:joi.array().items(joi.string())
        }).options({stripUnknown: true ,  abortEarly: false}),

        d_updateSchema: joi.object().keys({
            Name:joi.string().allow(null),
            Val:joi.string().allow(null),
        }).options({stripUnknown: true}),

        query_time_Schema: {
                //this is a get scheme - schema.validate(req[params] this is the :name) or (query - this is all fields(?time={{now}}) on the get url)
                time: joi.date().required()

            // headers: joi.object({
            //     Authorization: joi.string().token().required()
            // }).unknown() //unknown allows other headers like Content-Type etc.
        }


    },


};


// req.params) Checks route params, ex: /user/:id
//
// (req.query) Checks query string params, ex: ?id=12 Checks urlencoded body params

// const _validationOptions = {
//   abortEarly: false, // abort after the last validation error
//   allowUnknown: true, // allow unknown keys that will be ignored
//   stripUnknown: true // remove unknown keys from the validated data
// };
