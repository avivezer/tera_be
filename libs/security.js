const Crypto = require('crypto');
const ENV_CONFIG = require('../configurations/env_config');
const JWT = require('jsonwebtoken');
//this module is to encrypt secretive info such as phone number inside the database
const {randomString , generatePassword} = require('../libs/helperFunctions');

// Cipher Suites, key, and the iv
const pass = ENV_CONFIG.ENV_JWT_SECRET.substr(0,32); // key should be 32 bytes
const initialization_vector = "PZ9z3W7ZpK1BS4P2" ;// iv should be 16 length random string (can be public)

function encrypt(data) {

        try {
            let cipher = Crypto.createCipheriv('aes-256-cbc', pass, initialization_vector);
            let encrypted = cipher.update(data, "utf8", 'base64') + cipher.final('base64');
            return encrypted
        }
        catch (e) {
            console.log("error encrypt")
        }

}



function decrypt(data) {


    try {
        let decipher = Crypto.createDecipheriv("aes-256-cbc", pass , initialization_vector);
        let decrypted = decipher.update(data,  'base64', 'utf8' ) +  decipher.final('utf8');
        return decrypted;

    }
    catch (e) {
        console.log("error decrypt")
    }

}

function createTokenSignin(user, roletype) {
    return JWT.sign({
        iss: 'EyeSwift',   //issuer to be verify
        sub: user,                      // a UserID
        role: roletype,                    // role array

    }, ENV_CONFIG.ENV_JWT_SECRET, {expiresIn: "8h"});   //  //// "2 days", "10h", "7d" ot omit it for no expiration
}

function createToken2faSignin(user, roletype) {
    return JWT.sign({
        iss: 'EyeSwift',   //issuer to be verify
        sub: user,                      // a UserID
        role: roletype,                    // role array

    }, ENV_CONFIG.ENV_JWT_SECRET+1, {expiresIn: "3m"});
}

function createTokenSignEmail(user, role, body ,type ) {
    return JWT.sign({
        sub: user,                      // a UserID
        as_role: role,          //as_role is like role, but shuold not be the same like in signIn
        type:type,
        payload:body   // get body info

    }, ENV_CONFIG.ENV_JWT_SECRET+2, {expiresIn: "3h" , issuer: 'EyeSwift'});   //  //// "2 days", "10h", "7d" ot omit it for no expiration
}

function createTokenSelfRegistrationEmail(Email ,type ) {
    return JWT.sign({
        sub: Email,                      // a UserID
        type:type,
    }, ENV_CONFIG.ENV_JWT_SECRET+2, {expiresIn: "3h" , issuer: 'EyeSwift'});   //  //// "2 days", "10h", "7d" ot omit it for no expiration
}

function verifyTokenSignEmail(token) {
    return JWT.verify(
        token,
        ENV_CONFIG.ENV_JWT_SECRET+2,
        {issuer: "EyeSwift",
        ignoreExpiration:false,  // if to ignore exp validation
        });   //  //// "2 days", "10h", "7d" ot omit it for no expiration
}


/**
 * @return {string}
 */
function CreatePassword(req){
    let Password;
    if (req.value && req.value.body)
        Password = req.value.body.Password;
    if ( (!Password) && req.value && req.value.userinfo)
        Password = req.value.userinfo.Password;
    if (!Password)
        Password = generatePassword(8)

    return Password
}

module.exports = {encrypt , decrypt , createTokenSignin, createTokenSignEmail ,createToken2faSignin, createTokenSelfRegistrationEmail, verifyTokenSignEmail , CreatePassword};

// let x = "fsdfsdf@gmail.com"
// let xx = encrypt(x)
//// console.log(xx)
//// let xxx = JSON.stringify(xx)
// //console.log(xxx)
//// let bufferOriginal = Buffer.from(JSON.parse(xxx).data);
// //console.log(bufferOriginal);
//
// //let xxxx = decrypt(bufferOriginal)
//// console.log(xxxx)
// console.log(xx)
// let xxxx = decrypt(xx)
// console.log(xxxx)
