const joi = require('@hapi/joi');

module.exports = {

    //validate(value, schema, {escapeHtml: true}, [callback])
    ClinicValidateBody: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req.body);
            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['body'] = result.value;
            next();
        };
    },

    ClinicValidateQuery: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req["query"]);

            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['query'] = result.value;
            next();
        };
    },

    ClinicSchemas: {

        //everything else that needed and not in user management
        signupSchema: joi.object().keys({
            SiteName: joi.string().required(),
            SitePhoneNumber: joi.string().max(40).required(),
            SiteAddress: joi.string().max(40).required(),
            SiteEmail: joi.string().regex(/\S+@\S+\.\S+/).max(100).required(),
            CustomerID: joi.string().max(20).required(),
            //CustomerName: joi.string()
        }).options({stripUnknown: true , abortEarly: false}),

        updateSchema: joi.object().keys({

            SiteName: joi.string().alphanum(),
            CustomerID: joi.string().max(20),
            //CustomerName: joi.string(),
            Enabled:joi.number().valid(0,1),
            SitePhoneNumber: joi.string().regex(/^[+]?[0-9]+$/).max(20).allow(null),
            SiteState: joi.string().max(20).allow(null),
            SiteCountry: joi.string().max(20).allow(null),
            SiteEmail: joi.string().regex(/\S+@\S+\.\S+/).max(100),
            SiteCity: joi.string().max(20).allow(null),
            SiteStreet: joi.string().max(20).allow(null),
            SiteApartment: joi.string().max(20).allow(null),
            SiteZipCode: joi.string().max(20).allow(null),

        }).options({
            //remove unknown field from the validated body
            stripUnknown: true,
            // return all errors a payload contains, not just the first one Joi finds
            abortEarly: false}),


        setDeviceSchema: joi.object().keys({
            SerialNumber: joi.string().allow(null).required(),

        }).options({
            //remove unknown field from the validated body
            stripUnknown: true,
            // return all errors a payload contains, not just the first one Joi finds
            abortEarly: false}),

    },

    CustomerSchemas: {

        //everything else that needed and not in user management
        signupSchema: joi.object().keys({
            CustomerName: joi.string().required(),
            CRMLink: joi.string().required()
            //SiteEmail: joi.string().regex(/\S+@\S+\.\S+/).max(100).required(),
        }).options({stripUnknown: true , abortEarly: false}),

        updateSchema: joi.object().keys({

            CustomerName: joi.string().alphanum(),
            //SitePhoneNumber: joi.string().regex(/^[+]?[0-9]+$/).max(20).allow(null),
            CRMLink: joi.string(),
            Enabled:joi.number().valid(0,1)
            //SiteEmail: joi.string().regex(/\S+@\S+\.\S+/).max(100),
            
        }).options({
            //remove unknown field from the validated body
            stripUnknown: true,
            // return all errors a payload contains, not just the first one Joi finds
            abortEarly: false}),


        setDeviceSchema: joi.object().keys({
            SerialNumber: joi.string().allow(null).required(),

        }).options({
            //remove unknown field from the validated body
            stripUnknown: true,
            // return all errors a payload contains, not just the first one Joi finds
            abortEarly: false}),

    },
};
