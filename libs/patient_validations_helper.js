const joi = require('@hapi/joi');

module.exports = {

    PatientValidateBody: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req.body);
            if (result.error) {
                throw result.error;
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['body'] = result.value;
            next();
        };
    },

    PatientSchemas: {
        //everything else that needed and not in user management
        signupSchema: joi.object().keys({
            // do not put device sn on signup
            UserID:joi.any().strip(), // will remove it
            DoctorID:joi.any().strip(), // will remove it

        // fixme change to strip  stripUnknown allowUnknown

        }).options({allowUnknown:true, abortEarly: false,}),

        UpdateSchemaPatient: joi.object().keys({

            Notes: joi.string().allow(null),
            NextVisit: joi.string().allow(null),
            AccountStatus:joi.number().valid(3),
            // DeviceSN: joi.string(),
        }).options({stripUnknown: true ,  abortEarly: false}),


        sendMessagesSchema: joi.object().keys({
            Method :joi.string().valid("SMS" , "EMAIL" , "BOTH"),
            PatientsIDs:joi.array().items(joi.string())
        }).options({stripUnknown: true ,  abortEarly: false}),

    },


    VisitSchemas:{

        addNewVisit: joi.object().keys({
            // do not put device sn on signup
            VisitID:joi.any().strip(), // remove it
            UserID:joi.any().strip(), // remove it
            DoctorID:joi.any().strip(), // remove it

            AmblyopicEye : joi.number().required().valid(1,2).messages({
                'any.only': `AmblyopicEye valid: 1-left, 2-right`,
                'any.required': `AmblyopicEye required: 1-left, 2-right`,
            }),
            VisitDate:joi.date().iso().required(),


            // RightEyeDistanceVA:joi.number().required(),
            // LeftEyeDistanceVA:joi.number().required(),
            //
            // LeftEyeNearVA:joi.number().allow(null),
            // RightEyeNearVA:joi.number().allow(null),

            // fixme change to strip  stripUnknown allowUnknown

        }).options({allowUnknown:true, abortEarly: false,}),

    },


};
