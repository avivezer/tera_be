const AWS = require('aws-sdk');
const ENV_CONFIG = require('../configurations/env_config');


// Precedence region, client class constructor > global configuration object > AWS_SDK_LOAD_CONFIG
const S3 = new AWS.S3({region : ENV_CONFIG.ENV_AWS_REGION});
const TestBucketName = ENV_CONFIG.ENV_S3_BUCKET_NAME

const signedUrlExpireSeconds = 60 * 60  // expiry time in seconds

function S3getSignedUrl(testid ,filename ) {

    const url = S3.getSignedUrl('putObject', {
        Bucket: TestBucketName,
        Key: testid +"/input/" + filename +".zip",
        Expires: signedUrlExpireSeconds,
        ACL: 'bucket-owner-full-control',
        ContentType:'application/zip' //'binary/octet-stream'
    }); //for PUT it is extremely important to specify the ACL(Access Control List) and the ContentType

    return {[filename]:url}
}


function S3getSignedUrlD(testid ,filename ) {

    const url = S3.getSignedUrl('getObject', {
        Bucket: TestBucketName,
        Key: testid +"/input/" + filename +".zip",
        Expires: signedUrlExpireSeconds,
        ACL: 'bucket-owner-full-control',
        ContentType:'application/zip' //'binary/octet-stream'
    }); //for PUT it is extremely important to specify the ACL(Access Control List) and the ContentType

    return {[filename]:url}
}

async function S3createFolder(testid) {
    let params = {
        Bucket: TestBucketName,
        Key: testid + "/",
        ACL: 'bucket-owner-full-control',
        // Body: 'body does not matter'
    };
    let data
    try {
        data = await S3.putObject(params).promise() // upload
        console.log("Successfully created a folder on S3");
    } catch (e) {
        console.log("Error creating the folder: ", e);
        throw new Error(e)
    }
    return data
}

async function S3checkFile(testid , filename) {
    const params = {
        Bucket: TestBucketName,
        Key: testid +"/input/" + filename +".zip",     //if any sub folder-> path/of/the/folder.ext
    };                                                  //  "Key": "10/input/sal.zip"
    let data
    try {
        data =await S3.headObject(params).promise()
        console.log("File Found in S3")
    } catch (err) {
        console.log("File not Found ERROR : " + err.code)   ///"Forbidden" definitely means you don't have the appropriate "read" access
        throw new Error(err)                                    ///   "NotFound: null" = file dont exist
    }
    return data

}


async function S3PutFile(testid , filename , fileData,) {

    const params = {
        Bucket: TestBucketName,
        Key: testid +"/input/" + filename ,
        ContentType: "application/json",
        Body: fileData, ///  Body to be a string, Buffer, Stream, Blob, or typed array object"
        ACL: 'bucket-owner-full-control',
    };
    let data
    try {
        data = await S3.putObject(params).promise()
        console.log(`File uploaded successfully at ${data.Location}`)
    } catch (err) {
        console.log("File upload ERROR : " + err.code)
        throw new Error(err)
    }
    return data

}


module.exports = {

    S3getSignedUrl,
    S3createFolder,
    S3checkFile,
    S3PutFile,
    S3getSignedUrlD

}

