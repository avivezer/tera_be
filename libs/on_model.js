//create default admin after we import user model
//admin dont have RefInfo
//If the db.collection.update() with upsert: true had found a matching document, then MongoDB performs an update, applying the $set operation but ignoring the $setOnInsert operation.
const ENV_CONFIG = require('../configurations/env_config');

const {encrypt , decrypt } = require('./security');
const models  = require('../models'); // this is how you access all sequelize models
const ADMIN_USERNAME = ENV_CONFIG.ENV_EMAIL_INFO;
const SUADMIN_PASSWORD = ENV_CONFIG.ENV_SUADMIN_PASSWORD


async function InitDB() {
    let user , piiuser
    try {

         piiuser = await models.PII.upsert({ // upsert overrides record if exist
                UserID: ADMIN_USERNAME,
                Email: ADMIN_USERNAME,
                FirstName: "eye-swift",
                LastName: "super-admin",
            } , { returning: true } );

         user = await models.Users.upsert({  /// upsert doesnt call hooks
                UserIDpk: ADMIN_USERNAME,
                UserID: ADMIN_USERNAME,
                Role: ["EyeSwiftSuperAdmin"],
                Password: SUADMIN_PASSWORD,
                TwoFA:false, // // todo add phone number if true
                PassNeedsToChangeAt : null
         } , { returning: true  } );

        // seed
        await models.Counters.bulkCreate([
            {Name : "SiteID"},
            {Name : "AdminID"},
            {Name : "ECPID"},
            {Name : "PatientID"},
            {Name : "DeviceID"},
            {Name : "CustomerID"}

        ] , {ignoreDuplicates:true } )  // { ignore: true } for create, { ignoreDuplicates: true } for bulkCreate ,  if there happens to be a duplicate ID conflict, the rest of the query continues as it normally would (as opposed to throwing an error).

    }
    catch (err) {
        console.log(err)

        if ( 'boolean' !== typeof user || 'boolean' !== typeof piiuser){
            throw new Error("error create su admin");
        }


    }

}

/**
 * @return {Boolean}
 */
async function OnConnection(){
    ///sequelize.authenticate().then(function(errors) { console.log(errors) });
    let collectionNames

    //The first object is the result object, the second is the metadata object (containing affected rows etc) - but in mysql, those two are equal.
    collectionNames = await models.sequelize.query('show tables' ,{ type: models.Sequelize.QueryTypes.SELECT})
    // https://sequelize.org/master/manual/raw-queries.html

    if (collectionNames) {
        collectionNames.forEach(value => console.log(JSON.stringify(value, null, 0).replace(/\n/g, '')));
    }
    await InitDB()

    return true
}


module.exports = {

    OnConnection
}
