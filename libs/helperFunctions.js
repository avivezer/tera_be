const generator = require('generate-password');

/**
 * return new password
 * @param {number} [len] length of password.
 * @returns {string}
 */
const generatePassword = function(len){
    return  generator.generate({
        length: len,
        strict:true,
        lowercase:true,
        uppercase:true,
        numbers: true

        // exclude: '$^*()+_-=}{[]|:;"/?.><,`~',
        // symbols:true  ///'!@#$%^&*()+_-=}{[]|:;"/?.><,`~',
    });
}

/**
 * return new iso format string with UTC offset.
 * @param {number} [a] smaller.
 * @param {number} [b] bigger
 * @param {boolean} [inclusive=false] if set to true, uses inclusive , else inclusive.
 * @returns {boolean}
 */
Number.prototype.between = function(a, b, inclusive) {
    let min = Math.min(a, b),
        max = Math.max(a, b);

    return inclusive ? this >= min && this <= max : this > min && this < max;
};


/**
 * return correct number rounded by [places] decimal points.
 * @param {Number} [places] number to round.
 * @returns {Number}
 */
Number.prototype.myRound = function(places) {
    return +(Math.round(this + "e+" + places)  + "e-" + places);
}


/**
 * return new iso format string with UTC offset.
 * @param {boolean} [toURI=false] if send in URI Get method, need set to true.
 * @returns {string}
 */
Date.prototype.toIsoString = function(toURI = false) {
    let tzo = -this.getTimezoneOffset(),
        dif = tzo >= 0 ? toURI ? encodeURIComponent('+') : '+' : toURI ? encodeURIComponent('-') : '-',
        pad = function(num) {
            const norm = Math.floor(Math.abs(num));
            return (norm < 10 ? '0' : '') + norm;
        };
    return this.getFullYear() +
        '-' + pad(this.getMonth() + 1) +
        '-' + pad(this.getDate()) +
        'T' + pad(this.getHours()) +
        ':' + pad(this.getMinutes()) +
        ':' + pad(this.getSeconds()) +
        dif + pad(tzo / 60) +
        ':' + pad(tzo % 60);
};

/**
 * return random string without P and D prefix, reserved for doctor and patients
 * @param {number} [len] desired length.
 * @returns {string}
 */
function randomString(len) {
    let str = "";
    let possible = "ABCEFGHIJKLMNOQRSTUVWXYZabcdefghijklmnopqrstuvwxyz01234567890123456789";

    for (let i = 0; i < len; i++)
        str += possible.charAt(Math.floor(Math.random() * possible.length));
    return str;
}

function randomNumber(len , hypen) {
    let hypensign = hypen ? len / 2 : "nohypen"
    let str = "";
    let possible = "0123456789";
    for (let i = 0; i < len; i++) {
        if (i === hypensign)
            str += "-"
        str += possible.charAt(Math.floor(Math.random() * possible.length));   // random is inclusive of 0, but not 1
    }
    return str.toString();
}

module.exports = {randomString , generatePassword , randomNumber }


//remove field from all documents
//db.example.update({}, {$unset: {words:1}} , {multi: true});
