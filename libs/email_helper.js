const ENV_CONFIG = require('../configurations/env_config');
const AWS = require('aws-sdk');


const SES = new AWS.SES({region:'eu-central-1'}); // fixme
const moment = require('moment');

const EMAIL_ADDRESS = ENV_CONFIG.ENV_EMAIL_INFO;


async function listVerifiedEmailAddresses() {

    let res
    try {
        res = await SES.listVerifiedEmailAddresses().promise()
    } catch (e) {
        console.log("Error occurred verifyEmailAddress : ", e);
        return null
    }

    if (!res) {
        console.log("Error getting verifyEmailAddress response");
        return null
    }

    return res

}


async function sendFromEmailAddress(to, subject, message, myself) {
    const params = {
        Destination: {
            ToAddresses: myself ? [EMAIL_ADDRESS] : [to]
        },
        Message: {
            Body: {
                Html: {
                    Charset: 'UTF-8',
                    Data: message
                },
                ////replace Html attribute with the following if you want to send plain text emails.
                // Text: {
                //     Charset: "UTF-8",
                //     Data: message
                // }

            },
            Subject: {
                Charset: 'UTF-8',
                Data: subject
            }
        },

        Source: EMAIL_ADDRESS,  /* required */
        // ReplyToAddresses: [
        //     'EMAIL_ADDRESS'
        // ],
    };

    let res
    try {
        res = await SES.sendEmail(params).promise()
    } catch (e) {
        console.log("Error occurred sendEmail : ", e);
        return null
    }

    if (!res) {
        console.log("Error getting sendEmail response");
        return null
    }

    return res
}


async function sendFromEmailAddressRaw(to, messagebuffer, myself) {
    const params = {
        Destinations: [myself ? EMAIL_ADDRESS : to],
        RawMessage: {
            Data: Buffer.from(messagebuffer)// including subject and all
        },

        Source: EMAIL_ADDRESS,  /* required */

        // ReplyToAddresses: [
        //     'EMAIL_ADDRESS'
        // ],
    };

    let res
    try {
        res = await SES.sendRawEmail(params).promise()
    } catch (e) {
        console.log("Error occurred sendRawEmail : ", e);
        return null
    }

    if (!res) {
        console.log("Error getting sendRawEmail response");
        return null
    }

    return res
}


// [ {Name:String , Val:{} , _id: false }], //Burning : True
function formatvalues(arr) {
    let ans = []
    for (const [index, element] of arr.entries()){
        if (element.Val === true || (element.Name.toLowerCase().startsWith("other") && element.Val!=="" )  ) {
            ans.push(element.Name);
            ans.push("\n")
        }
        // if(index === arr.length -1)
        //     ans.push("\n")
    }

    return ans
}





// for pdf sending
async function sendmailraw(buffer, to, subject, testid) {

    var ses_mail = "From: 'GYNI support' <" + EMAIL_ADDRESS + ">\n";
    ses_mail = ses_mail + "To: " + to + "\n";
    ses_mail = ses_mail + "Subject: " + subject + "\n";
    ses_mail = ses_mail + "MIME-Version: 1.0\n";
    ses_mail = ses_mail + "Content-Type: multipart/mixed; boundary=\"NextPart\"\n\n";
    ses_mail = ses_mail + "--NextPart\n";
    ses_mail = ses_mail + "Content-Type: text/html; charset=us-ascii\n\n";
    ses_mail = ses_mail + "Gyntools test id "+ testid +" result" +  ".\n\n";
    ses_mail = ses_mail + "--NextPart\n";
    ses_mail += "Content-Type: application/octet-stream; name=\"" + testid + "-test-result.pdf\"\n";
    ses_mail += "Content-Transfer-Encoding: base64\n";
    ses_mail += "Content-Disposition: attachment\n\n";
    ses_mail = ses_mail + buffer.toString('base64').replace(/([^\0]{76})/g, "$1\n") + "\n\n";
    ses_mail = ses_mail + "--NextPart--";
    let x = await sendFromEmailAddressRaw(to, ses_mail)

    return x
}


function emailBodyFormaterJson(req_body) {

    let requestBody = JSON.stringify(req_body, null, "\t")
    return requestBody

}


function CreateNewAdmin(body) {


    let emailBody =
        `Welcome to EyeSwift™ ${body.PIIProfile.FirstName} <br>` +
        ` <br> ` +
        `A new administrator user was created in EyeSwift™ system with the following details :<br> ` +
        `First Name: ${body.PIIProfile.FirstName}  <br> ` +
        `Last Name: ${body.PIIProfile.LastName}  <br> ` +
        `Email address: ${body.PIIProfile.Email}  <br> ` +
        `Account password: ${body.Password}  <br> ` +
        ` <br> ` +


        "You are welcome to use EyeSwift™ <br>" +
        "Thank you <br>" +
        "EyeSwift™ support <br>"

    return emailBody

}



function CreateNewDoctor(body) {


    let emailBody =
        `Welcome to EyeSwift™ ${body.PIIProfile.FirstName} ${body.PIIProfile.LastName} <br>` +
        ` <br> ` +
        `A new Eye Care provider user was created in EyeSwift™ system with the following details :<br> ` +
        `First Name: ${body.PIIProfile.FirstName}  <br> ` +
        `Last Name: ${body.PIIProfile.LastName}  <br> ` +
        `Email address: ${body.PIIProfile.Email}  <br> ` +
        `Account password: ${body.Password}  <br> ` +
        ` <br> ` +

        (body.Site?
            "You were added to the following Site: <br>" +
            `Clinic name: ${body.Site.SiteName} <br> ` +
            `Clinic Email: ${body.Site.SiteEmail}  <br> ` +
            `Clinic's phone number: ${body.Site.SitePhoneNumber || ""} <br> ` +
            `Clinic address:  <br> ` +
                `   Address: ${body.Site.SiteCity || ""}, ${body.Site.SiteStreet || ""}, ${body.Site.SiteApartment || ""}<br> ` +
                `   Zip Code: ${body.Site.SiteZipCode} <br> ` +
                `   Country: ${body.Site.SiteCountry || ""} <br> `
            :"") +

        "You are welcome to use EyeSwift™ <br>" +
        "Thank you <br>" +
        "EyeSwift™ support <br>"

    return emailBody

}

function EmailChangedNotification(body) {


    let emailBody =
        `Hello ${body.FirstName} ${body.LastName}` +
        ` <br> ` +
        `Your new user for EyeSwift™ system is:<br> ` +
        `${body.Email}  <br> ` +

        "You are welcome to use EyeSwift™ <br>" +
        "Thank you <br>" +
        "EyeSwift™ support <br>"

    return emailBody

}


function deletedAccountNotification(body) {


    let emailBody =
        `Hello ${body.FirstName} ${body.LastName}` +
        ` <br> ` +
        `your EyeSwift™ account has been deleted<br> ` +

        "Thank you <br>" +
        "EyeSwift™ support <br>"

    return emailBody

}


function EmailChangedPatientNotification(body) {


    let emailBody =
        `Hello ${body.PIIProfile.FirstName} ${body.PIIProfile.LastName}` +
        ` <br> ` +
        `EyeSwift™ system will contact you using your new email:<br> ` +
        `${body.PIIProfile.Email}  <br> ` +

        "You are welcome to use EyeSwift™ <br>" +
        "Thank you <br>" +
        "EyeSwift™ support <br>"

    return emailBody

}


function CreateNewOrderLogisticCenter(body) {

    let items = {
        // device : {mkt:"NSP-00200-R01" , name:"EyeSwift-CS100-Device"},
        glaces : { 1 : {mkt:"NSP-00239-R01" , name:"EyeSwift glasses- model B"}, // left
                   2 :  {mkt:"NSP-00240-R01" , name:"EyeSwift glasses- model A"}  // right
                }
    };

    // let leftOrRight = body.Visit.AmblyopicEye


    let emailBody =
        `Dear logistics team, <br>` +
        ` <br> ` +
        `Please find below the fulfillment details for a customer's recent EyeSwift™ purchase:<br> ` +
        `Customer Name: ${body.PIIProfile.FirstName} ${body.PIIProfile.LastName} <br> ` +
        `Customer Email: ${body.PIIProfile.Email}  <br> ` +
        `Customer Phone Number: ${body.PIIProfile.PhoneNumber}  <br> ` +
        `Customer Mailing Address:<br> ` +
        `   Address: ${body.PIIProfile.City || ""}, ${body.PIIProfile.Street || ""}, ${body.PIIProfile.Apartment || ""}<br> ` +
        `   Zip Code: ${body.PIIProfile.ZIPCode || ""} <br> ` +
        `   Country: ${body.PIIProfile.Country || ""} <br> `+
        ` <br> ` +

        // "Please provide the following items:<br>" +
        // `    ${items.device.mkt} ${items.device.name} <br> ` +
        // `    ${items.glaces[leftOrRight].mkt} ${items.glaces[leftOrRight].name} <br> ` +

        ` <br> ` +
        "We appreciate your help in fulfilling this order as quickly as possible.<br>" +
        "Thank you, <br>" +
        "The NovaSight Team<br>"

    return emailBody

}



function CreateNewPatient(body) {

    let emailBody =
        `Welcome to EyeSwift™ ${body.PIIProfile.FirstName} ${body.PIIProfile.LastName} <br>` +
        ` <br> ` +
        `Congratulations on the purchase of your EyeSwift™ amblyopia treatment system.<br> ` +
        `You can expect to receive the system within 3 business days from the date of your order.<br> ` +
        `Please note your login details: <br> ` +
        `Username: ${body.Patient.PatientID}  <br> ` +
        `Account password: ${body.Password}  <br> ` +
        ` <br> ` +

        "Please take note of your login as you’ll need it each time you access the system.<br>" +
        "Should you encounter any technical difficulties, please contact your local eye care professional directly,<br>" +
        "or NovaSight at cs-support@nova-sight.com and by phone at +97253-4812642, +97253-4813596. <br>" +
        "NovaSight’s goal is to revolutionize pediatric vision care, enabling children worldwide to live their dreams.\n" +
        "We hope you enjoy your purchase. <br>" +
        "Thank you <br>" +
        "EyeSwift™ support <br>"

    return emailBody

}



function patientReminder(body) {
///// Subject: Practice makes perfect! EyeSwift™ Reminder
    let emailBody =
        `Hello ${body.FirstName} ${body.LastName} <br>` +
        ` <br> ` +
        `Practice makes perfect! We have noticed that ${body.FirstName} has not been following their recommended weekly treatment.<br> ` +
        `Please note that not following the recommended treatment protocol can affect treatment progress and effectiveness..<br> ` +
        `For further information or assistance please contact your eye care professional.<br> ` +
        ` If you are experiencing any technical difficulties please contact your eye care professional directly,<br> ` +
        "or NovaSight at cs-support@nova-sight.com and by phone at +97253-4812642, +97253-4813596. <br>" +
        ` <br> ` +

        "Thank you <br>" +
        "EyeSwift™ support <br>"

    return emailBody

}




function resetPasswordPatientEmail(req, token, userid) {

    // let currentFrontendUrl = ENV_CONFIG.ENV_FRONTEND_URL
    // let confirmationUrl = currentFrontendUrl + `/forgot/?token=${token}`;

    let currentBackendUrl = req.protocol + '://' + req.get('host') + req.baseUrl;
    let confirmationUrl = currentBackendUrl + `/forgetPass/ResetConfirm/?token=${token}`;

    let linker = `<a href="${confirmationUrl}"><span>reset password</span></a>`;

    let emailBody = `Hello ${userid} <br>` +
        "this massage sent to you upon request to reset your password at EyeSwift™ account <br>" +
        "if you didnt request that, we are apologize and please ignore this email<br> " +
        "otherwise, click bellow link to reset the password<br> " +
        "your link: <br>" +
        linker;

    return emailBody

}

function selfRegisterEmail(req, token, userid) {

    // let currentFrontendUrl = ENV_CONFIG.ENV_FRONTEND_URL
    // let confirmationUrl = currentFrontendUrl + `/forgot/?token=${token}`;

    let currentBackendUrl = req.protocol + '://' + req.get('host') + req.baseUrl;
    let confirmationUrl = currentBackendUrl + `/users/self/Confirm/?token=${token}`;

    let linker = `<a href="${confirmationUrl}"><span>Confirm Registration</span></a>`;

    let emailBody = `Hello ${userid} <br>` +
        "this massage sent to you upon request to register an EyeSwift™ account <br>" +
        "if you didnt request that, we are apologize and please ignore this email<br> " +
        "otherwise, click bellow link to confirm your registration<br> " +
        "your link: <br>" +
        linker;

    return emailBody

}






module.exports = {

    listVerifiedEmailAddresses,
    sendFromEmailAddress,
    sendFromEmailAddressRaw,

    CreateNewAdmin,
    CreateNewDoctor,
    CreateNewPatient,
    CreateNewOrderLogisticCenter,
    patientReminder,

    EmailChangedNotification,
    EmailChangedPatientNotification,
    deletedAccountNotification,

    resetPasswordPatientEmail,
    selfRegisterEmail


}

