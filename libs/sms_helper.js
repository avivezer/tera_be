const ENV_CONFIG = require('../configurations/env_config');
const AWS = require('aws-sdk');


const SNS = new AWS.SNS({region:'eu-central-1'}); // set region fixme


// aws need to set sms quota
async function sendToMobile(message, phoneNumber) {
    //from - up to 11 characters. Accepted characters include both upper- and lower-case ASCII letters,
    // the digits 0 through 9, and space: A-Z, a-z, 0-9. They may not be only numbers.
    let from = "NOVASIGHT";

    const params = {
        Message: message,
        PhoneNumber: phoneNumber,   // E.164 format - as +1001XXX5550100.
        MessageAttributes: {                          // +972 52 4658229
            'AWS.SNS.SMS.SenderID': {
                DataType: 'String',
                StringValue: from
            },
            'AWS.SNS.SMS.SMSType': {
                DataType: 'String',
                StringValue: 'Transactional'  ///     'DefaultSMSType': 'Transactional', /* highest reliability *///'DefaultSMSType': 'Promotional' /* lowest cost */
            }
        },
    };

    let res
    try {
        res = await SNS.publish(params).promise()
    } catch (e) {
        console.log("Error occurred sendSMS : ", e);
        return null
    }

    if (!res) {
        console.log("Error getting sendSMS response");
        return null
    }

    console.log("sended sms msg to " + phoneNumber)
    return res
}


function patientRemainderSMS( name) {


    let SmsBody = "EyeSwift Treatment Reminder! \n" +
        name  + " has not been following treatment protocol.\n" +
        "For better results, it’s important to practice. \n"+
        "Questions? support@nova-sight.com";


    return SmsBody

}


function patientResetSMS( Username,Code) {


    let SmsBody = "Welcome to !\n " +
        "Your Username: " +Username  +
        "\nPassword: " +Code

    return SmsBody

}


function TwoFaSMS(Code) {

    let SmsBody = "Your Code\n " + Code.toString()

    return SmsBody
}


module.exports = {
    sendToMobile,
    patientRemainderSMS,
    patientResetSMS,
    TwoFaSMS
}

