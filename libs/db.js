const ENV_CONFIG = require('../configurations/env_config');
const on_model = require('../libs/on_model');

/// first open db connection ,
// only then , open server listner
const models = require('../models'); // INDEX MODELS

// models.sequelize.afterConnect( (conn ,xonn )  => {
// console.log()
// });


///perform an SQL query to the database. Note that this changes only the table in the database
let options = {
    force:true,
    // alter:true /// FIXME alter won't drop constraints
}

//production protection
if (process.env.NODE_ENV === "production") {
    options.force=false
}

console.log('force flag is:'+  options.force.toString() );


// but first the migrate script is called
//syncs models to the database -
// create tables that do not exists. force: true, it will drop the table that exists and recreate them from the model definition.
models.sequelize.query('SHOW DATABASES')
    .then(function(e) {
        // console.log(e)
        return models.sequelize.query('SET FOREIGN_KEY_CHECKS = 0')})  /// (session based) state.
    .then(function(e) {
        return models.sequelize.sync(options)})
    .then(function(e) {
        // console.log(e)
        console.log("connection db successful")
        // must return - Warning: a promise was created in a handler at but was not returned from it [bluebird]
        return models.sequelize.query('SET FOREIGN_KEY_CHECKS = 1')})
    .then(function(e){
        console.log('Database sync completed.');
        return on_model.OnConnection()})
    .then(function(e) {
        console.log("on_model sync db successful")
    }).catch(function (e) {
    console.log("sync error")
    console.log("error connecting db error")
    console.error(e)
    process.exit(1);

});


/// call sequelize.close() (which is asynchronous and returns a Promise).
