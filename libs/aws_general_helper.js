const ENV_CONFIG = require('../configurations/env_config');
const AWS = require('aws-sdk');
const sts = new AWS.STS();

///https://aws.amazon.com/blogs/security/iam-policies-and-bucket-policies-and-acls-oh-my-controlling-access-to-s3-resources/
///https://aws.amazon.com/premiumsupport/knowledge-center/s3-folder-user-access/


// this is not bucket policy , The “Principal” (in bucket policy )element is unnecessary in an IAM policy, because the principal is by default the entity that the IAM policy is attached to.
function createBucketLogsPolicy(client  ) {
    let policy = {
        "Version": "2012-10-17",
        "Statement": [
            {
                "Sid": "AllowStatement1",
                "Action": ["s3:ListAllMyBuckets", "s3:GetBucketLocation"],
                "Effect": "Allow",
                "Resource": ["arn:aws:s3:::*"]
            },
            {
                "Sid": "AllowStatement2B",
                "Action": ["s3:ListBucket"],
                "Effect": "Allow",
                "Resource": [`arn:aws:s3:::${ENV_CONFIG.ENV_S3_BUCKET_NAME}`],
                "Condition":{"StringEquals":{"s3:prefix":["",`${client}`],"s3:delimiter":["/"]}}
            },
            {
                "Sid": "AllowStatement3",
                "Action": ["s3:ListBucket"],
                "Effect": "Allow",
                "Resource": [`arn:aws:s3:::${ENV_CONFIG.ENV_S3_BUCKET_NAME}`],
                "Condition":{"StringLike":{"s3:prefix":[`${client}/*`]}}
            },
            {
                "Effect": "Allow",
                "Action":  [
                    "s3:GetObject",
                    "s3:PutObject"
                ],
                "Resource": [ `arn:aws:s3:::${ENV_CONFIG.ENV_S3_BUCKET_NAME}/${client}/*`
                ]
            },

            /// end policy for putting logs , now policy to get versions

            {
                "Sid": "AllowStatement5",
                "Action": ["s3:ListBucket"],
                "Effect": "Allow",
                "Resource": [`arn:aws:s3:::${ENV_CONFIG.ENV_S3_Versions_BUCKET_NAME}`], // no /*
            },

            {
                "Effect": "Allow",
                "Action":  [
                    "s3:GetObject"],
                "Resource": [ `arn:aws:s3:::${ENV_CONFIG.ENV_S3_Versions_BUCKET_NAME}/*`
                ]
            }




        ]};

    return policy
}


async function assumeRole(client ) {

    if (!client )
        return null
    let data = null;
    try {
        data = await sts.assumeRole({
            RoleArn: ENV_CONFIG.ENV_S3_Role,  // // dont forget to trust arn:aws:iam::387555613126:user/eyeswift_medix_admin1
            RoleSessionName: client,
            DurationSeconds: 60 * 60 * 12,
            Policy: JSON.stringify(createBucketLogsPolicy(client)), /// intersection of role's identity-based policy and this session policy
        }).promise()
    }catch (err) {
        console.log(err, err.stack); // an error occurred
        return null
    }
    if (data) {
        console.log("created s3 cred to " + client + ", device: " )
        return data
    }
    else
        return null

}





module.exports = {

    assumeRole
}
