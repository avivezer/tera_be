const joi = require('@hapi/joi');
//https://hapi.dev/module/joi/api/?v=17.1.1

/// password ^(?=.*[a-z])(?=.*[A-Z])(?=.*[0-9])(?=.*[!@#\$%\^&\*])(?=.{8,})
/// https://www.npmjs.com/package/check-password-strength

module.exports = {

    ValidateInfo: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req.body);
            if (result.error) {
                throw result.error
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['userinfo'] = result.value;
            next();
        };
    },


    ValidateBody: (schema) => {
        return (req, res, next) => {
            const result = schema.validate(req.body);
            if (result.error) {
                throw result.error;
            }

            if (!req.value) { // req.value will hold the validated body
                req.value = {};
            }

            req.value['body'] = result.value;
            next();
        };
    },

    // PII
    UserSchemas: {

        AllWebUsersSignInSchema: joi.object().keys({
            Email:joi.string().regex(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-_]+\.?[a-zA-Z0-9-.]*$/).max(100).required(),
            Password: joi.string().required()
                .regex(/^(?=^.{8,20}$)(?=.*\d)(?=.*[!@#%^&*]*)(?![.\n])(?!.*[${}\/<>\[\]\"\'\`])(?=.*[A-Z])(?=.*[a-z]).*$/)
                .messages({
                    "string.pattern.base": `"Password" failed to match pattern`,
                })

        }).options({stripUnknown: true, abortEarly: false}),

        AllDeviceUsersSignInSchema: joi.object().keys({
            Email: joi.string().required(),
            Password: joi.string().required(),
            ESVersion:joi.string().required(),
            TVVersion:joi.string().required(),
            EmitterFirmwareVersion:joi.string().required(),
            WindowsVersion:joi.string().required(),
            ETFirmwareVersion:joi.string().required(),

        }).options({stripUnknown: true, abortEarly: false}),

        twoFAUsersSignInSchema: joi.object().keys({
            TwoFaCode: joi.string().max(10).alphanum().required(),
        }).options({stripUnknown: true , abortEarly: false}),

        AdminSignUpSchema: joi.object().keys({
            FirstName: joi.string().max(40).required(),
            LastName: joi.string().max(40).required(),
            Email:joi.string().regex(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-_]+\.?[a-zA-Z0-9-.]*$/).max(100).required(),
            PhoneNumber: joi.string().max(20).regex(/^[+]?[0-9]+$/).required(),
            AdminPosition: joi.required(),
            TwoFA:joi.boolean().required(),

        }).options({stripUnknown: true}),

        AdminUpdateSchema: joi.object().keys({
            FirstName: joi.string().max(40),
            LastName: joi.string().max(40),
            Password: joi.string().max(20).regex(/^(?=^.{8,20}$)(?=.*\d)(?=.*[!@#%^&*]*)(?![.\n])(?!.*[${}\/<>\[\]\"\'\`])(?=.*[A-Z])(?=.*[a-z]).*$/),
            Email: joi.string().regex(/\S+@\S+\.\S+/).max(100),
            PhoneNumber: joi.string().max(20).regex(/^[+]?[0-9]+$/),
            Enabled:joi.number().valid(0,1),
            TwoFA:joi.boolean(),
            AdminPosition: joi.string(),
        }).options({stripUnknown: true}),


        DoctorSignupSchema: joi.object().keys({
            SiteID: joi.string().required(),
            FirstName: joi.string().required(),
            LastName: joi.string().required(),
            Email:joi.string().regex(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-_]+\.?[a-zA-Z0-9-.]*$/).max(100).required(),
            PhoneNumber: joi.string().max(20).required().regex(/^[+]?[0-9]+$/),
            Profession: joi.number().valid(1,2,3,4,5,6,7).messages({ /// https://github.com/hapijs/joi/issues/2158
                'any.only':  `Profession valid: 1-MD, 2-Optometrist, 3-Ophthalmologist, 4-Orthoptist, 5-Optician, 6-Administrative, 7-Other`,
                'any.required':  `Profession required: 1-MD, 2-Optometrist, 3-Ophthalmologist, 4-Orthoptist, 5-Optician, 6-Administrative, 7-Other`,
            }).allow(null),
            MedicalLicense: joi.string().allow(null),
            DefaultVAUnit: joi.string().allow(null),
            SocialID: joi.string().allow(null),
            Notes: joi.string().allow(null),
            SiteAdmin: joi.boolean(),
            CustomerAdmin: joi.boolean(),
            TwoFA: joi.boolean()

        }).options({stripUnknown: true, abortEarly: false}),


        DoctorUpdateSchema: joi.object().keys({
            SiteID: joi.string(),
            FirstName: joi.string(),
            LastName: joi.string(),
            Email: joi.string().regex(/\S+@\S+\.\S+/).max(100),
            PhoneNumber: joi.string(),
            Profession: joi.number().valid(1,2,3,4,5,6,7).messages({ /// https://github.com/hapijs/joi/issues/2158
                'any.only': `Profession valid: 1-MD, 2-Optometrist, 3-Ophthalmologist, 4-Orthoptist, 5-Optician, 6-Administrative, 7-Other`,
                'any.required':  `Profession required: 1-MD, 2-Optometrist, 3-Ophthalmologist, 4-Orthoptist, 5-Optician, 6-Administrative, 7-Other`,
            }).allow(null),
            MedicalLicense: joi.string().allow(null),
            SocialID: joi.string().allow(null),
            DefaultVAUnit: joi.string().allow(null),
            Notes: joi.string().allow(null),

            Password: joi.string().regex(/^[a-zA-Z0-9!#$%&?]{6,12}$/),
            Enabled:joi.number().valid(0,1),
            SiteAdmin:joi.boolean(),
            CustomerAdmin: joi.boolean(),
            TwoFA: joi.boolean()

        }).options({stripUnknown: true, abortEarly: false}),


        // signupSchema: signup_schema pii
        PatientSignupSchema: joi.object().keys({
            FirstName: joi.string().required(),
            LastName: joi.string().required(),
            Email:joi.string().regex(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-_]+\.?[a-zA-Z0-9-.]*$/).max(100).required(),
            Gender: joi.number().valid(1,2,3).required().messages({ /// https://github.com/hapijs/joi/issues/2158
                'any.only': `Gender valid: 1-Male, 2-Female , 3-Other`,
                'any.required': `Gender required: 1-Male, 2-Female , 3-Other`,
            }),
            PhoneNumber: joi.string().max(20).regex(/^[+]?[0-9]+$/),
            Birthdate: joi.string().required(),
            Country:  joi.string().allow(null),
            State:  joi.string().allow(null),
            City:  joi.string().allow(null),
            Street:  joi.string().allow(null),
            Apartment:  joi.string().allow(null),
            ZIPCode:  joi.string().allow(null),
            SocialID: joi.string().allow(null),
            Notes: joi.string().allow(null),
            DeviceSN: joi.string(),
        }).options({
            //remove unknown field from the validated body
            stripUnknown: true,
            // return all errors a payload contains, not just the first one Joi finds
            abortEarly: false
        }),

        // pii
        PatientUpdateSchema: joi.object().keys({
            FirstName: joi.string(),
            LastName: joi.string(),
            Email: joi.string().regex(/\S+@\S+\.\S+/).max(100),
            Gender: joi.number().valid(1,2,3).messages({ /// https://github.com/hapijs/joi/issues/2158
                'any.only': `Gender valid: 1-Male, 2-Female , 3-Other`,
                'any.required': `Gender required: 1-Male, 2-Female , 3-Other`,
            }).allow(null),
            PhoneNumber: joi.string().allow(null),
            Birthdate: joi.string().allow(null),
            Country:  joi.string().allow(null),
            State:  joi.string().allow(null),
            City:  joi.string().allow(null),
            Street:  joi.string().allow(null),
            Apartment:  joi.string().allow(null),
            ZIPCode:  joi.string().allow(null),
            SocialID: joi.string().allow(null),
            Enabled:joi.number().valid(0,1)

        }).options({
            //remove unknown field from the validated body
            stripUnknown: true,
            // return all errors a payload contains, not just the first one Joi finds
            abortEarly: false
        }),


        webForgotSchema: joi.object().keys({
            Email:joi.string().max(100).required(),
        }).options({stripUnknown: true, abortEarly:false}),

        ChangePassSchema: joi.object().keys({
            newPassword: joi.string().required().regex(/^(?=^.{8,20}$)(?=.*\d)(?=.*[!@#%^&*]*)(?![.\n])(?!.*[${}\/<>\[\]\"\'\`])(?=.*[A-Z])(?=.*[a-z]).*$/),
        }).options({stripUnknown: true , abortEarly: false}),

        ChangeLanguageSchema: joi.object().keys({
            language: joi.string().required().min(2).max(3)
        }).options({stripUnknown: true , abortEarly: false}),

        registration_schema: joi.object().keys({
            SiteName: joi.string().required(),
            SitePhoneNumber: joi.string().max(40),
            SiteEmail: joi.string().regex(/\S+@\S+\.\S+/).max(100).required(),
            State: joi.string().max(40).allow(null),
            Country: joi.string().max(40).required(),
            City: joi.string().max(40).allow(null),
            Street: joi.string().max(40).allow(null),
            Apartment: joi.string().max(40).allow(null),
            ZipCode: joi.string().max(20).allow(null),
            FirstName: joi.string().required(),
            LastName: joi.string().required(),
            Email:joi.string().regex(/^[a-zA-Z0-9_.+-]+@[a-zA-Z0-9-_]+\.?[a-zA-Z0-9-.]*$/).max(100).required(),
            MedicalLicense: joi.string().allow(null),
            PhoneNumber: joi.string().max(20).regex(/^[+]?[0-9]+$/).required(),
            Profession: joi.number().valid(1,2,3,4,5,6,7).messages({ /// https://github.com/hapijs/joi/issues/2158
                'any.only':  `Profession valid: 1-MD, 2-Optometrist, 3-Ophthalmologist, 4-Orthoptist, 5-Optician, 6-Administrative, 7-Other`,
                'any.required': `Profession required: 1-MD, 2-Optometrist, 3-Ophthalmologist, 4-Orthoptist, 5-Optician, 6-Administrative, 7-Other`,
            }).required(),

        }).options({stripUnknown: true , abortEarly: false})
        ,
    },


};
