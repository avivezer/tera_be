const ENV_CONFIG = require('../configurations/env_config');
const {encrypt, decrypt, CreatePassword} = require('../libs/security');
const SEShelper = require('../libs/email_helper');
const models = require('../models'); // this is how you access all sequelize models
const moment = require('moment');
const {patientRemainderSMS , sendToMobile} = require('../libs/sms_helper');


module.exports = {

    AdminSignUp: async (req, res, next) => {

        let adminid = await models.Counters.getCount("AdminID");
        req.value.userinfo.UserID = adminid
        req.value.userinfo.UserIDpk = req.value.userinfo.UserID

        let user, adminPII
        try {

            adminPII = await models.PII.create(req.value.userinfo, {context: {req: req, sourceUserID:adminid}}); // pass options;

            let Password = CreatePassword(req);
            req.value.userinfo.Role = ["EyeSwiftAdmin"];
            req.value.userinfo.Password = Password;
            user = await models.Users.create(req.value.userinfo);

            let ans = Object.assign({}, user.toJSON(), {PIIProfile: adminPII.toJSON()}, {Password: Password})

            // // req.withoutMail = true; // fixme // takes time
            // if (!(req.withoutMail || req.body.withoutMail)) {
            //     let emailBody = SEShelper.CreateNewAdmin(ans)
            //     let ansmail = await SEShelper.sendFromEmailAddress(
            //         adminPII.Email,
            //         'Congratulations for your new EyeSwift™ amblyopia treatment system account',
            //         emailBody);

            //     if (!(ansmail && ansmail.MessageId))
            //         return res.status(422).send("invalid email request")
            // }

            if (res)
                res.send(ans);
            else
                return ans;


        } catch (e) {
            console.error(e)

            if (user)
                await user.destroy();
            if (adminPII)
                await adminPII.destroy();

            throw e;
        }
    },


    SiteSignUp: async (req, res, next) => { // facility with user creation

        let siteid, site;
        siteid = await models.Counters.getCount("SiteID");
        req.value.body.SiteID = siteid
        site = await models.Sites.create(req.value.body);
        let pending = await models.PendingUsers.findOne({where: {SiteName: site.SiteName}})
        if (pending) await pending.destroy()
        if (res)
            res.send(site);
        else
            return site;

    },

    CustomerSignUp: async (req, res, next) => { // facility with user creation

        let customerid, customer;
        customerid = await models.Counters.getCount("CustomerID");
        req.value.body.CustomerID = customerid
        customer = await models.Customers.create(req.value.body);
        let pending = await models.PendingCustomers.findOne({where: {CustomerName: customer.CustomerName}})
        if (pending) await pending.destroy()
        if (res)
            res.send(customer);
        else
            return customer;

    },

    DoctorSignUp: async (req, res, next) => {

        let siteToFind = req.value.userinfo.SiteID
        let site = await models.Sites.findOne({where: {SiteID: siteToFind}})
        if (!site)
            throw new Error("no such SiteID");

        let doctorid = await models.Counters.getCount("ECPID");
        req.value.userinfo.UserID = doctorid
        req.value.userinfo.UserIDpk = req.value.userinfo.UserID

        let user, doctorPII, doctor;
        try {

            doctorPII = await models.PII.create(req.value.userinfo, {context: {req: req, sourceUserID:doctorid}}); // pass options;
            doctor = await models.EyeCareProviders.create(req.value.userinfo); // drops additional fields if not in scheme

            let Password = CreatePassword(req);
            req.value.userinfo.Role = ["Doctor"];
            req.value.userinfo.Password = Password;
            user = await models.Users.create(req.value.userinfo);

            let ans = Object.assign({}, user.toJSON(), {Doctor: doctor.toJSON()}, {PIIProfile: doctorPII.toJSON()}, {Site: site.toJSON()}, {Password: Password})

            // // req.withoutMail = true; // fixme
            // if (!(req.withoutMail || req.body.withoutMail)) {  //send email only if user successfully created and withoutMail is null
            //     let emailBody = SEShelper.CreateNewDoctor(ans)
            //     let ansmail = await SEShelper.sendFromEmailAddress( // to doctor,
            //         doctorPII.Email,
            //         'Congratulations for your new EyeSwift™ amblyopia treatment system account',
            //         emailBody);

            //     if (!(ansmail && ansmail.MessageId))
            //         return res.status(422).send("invalid email request")
            // }

            if (res)
                res.send(ans);
            else
                return ans;


        } catch (e) {
            console.error(e)

            if (user)
                await user.destroy();
            if (doctor)
                await doctor.destroy();
            if (doctorPII)
                await doctorPII.destroy();

            throw e;
        }
    },


    //dynamic fields and clinical trail info
    d_u_init: async (req, res, next) => {

        let DupdateArray = Object.keys(req.body)
        let promise_DupdateArray = DupdateArray.map(function (n) {
            return GeneralData.findOneAndUpdate({Name: n}, {Val: req.body[n]},
                {upsert: true, new: true, fields: {_id: 0, __v: 0}})
        });

        // fixme only for clinical trail
        let promise_DiagnosisArray = []
        if (req.body["Diagnosis"]) {
            let diagnosisArray = req.body["Diagnosis"];
            // promise_DiagnosisArray = diagnosisArray.map(function (n) {
            //     return ClinicalTrails.findOneAndUpdate({Name: n.Name}, n, {upsert: true, new: true, fields: {_id: 0 , __v:0}})
            // });
        }

        let all = promise_DupdateArray.concat(promise_DiagnosisArray);
        let docs = await Promise.all(all)
        if (docs)
            res.status(200).send(docs);
        else
            throw new Error("error create fields")
    },

    d_u_add: async (req, res, next) => {
        let Name = req.body['Name'];
        let Val = req.body.Val;

        let doc = await GeneralData.findOneAndUpdate({Name: Name}, {"Val": Val}, {
            upsert: true,
            new: true,
            fields: {_id: 0, __v: 0}
        });
        if (doc)
            res.status(200).send(doc);
        else
            res.status(400).send()

    },

    d_u_getAll: async (req, res, next) => {
        const ans = await GeneralData.find({}, {_id: 0, __v: 0}); // the await make the .then call which is like exec().Something to note when using Promises in
        // combination with Mongoose async operations is that Mongoose queries are not Promises.exec() will return a promise if no callback is provided.
        // Queries do return a thenable, but if you need a real fully-fledged Promise you should use the exec method.
        if (!ans || (Array.isArray(ans) && ans.length === 0))
            throw new Error("no d_u fields in db")

        res.send(ans);
    },


    getAllSites: async (req, res, next) => {

        let sites = await models.Sites.findAll({
            include: [
                {
                    model: models.EyeCareProviders,
                    attributes: [],
                    required: false,// true is inner join instead  of left join
                    include: [],
                },
                {
                    model: models.Devices,
                    attributes: ['SiteID'],
                    required: false,// true is inner join instead  of left join
                    include: [],
                },
                {
                    model: models.Customers
                }
            ],
/// this query will return same each site for each one of the patient , so we must distinct the ECP
            // SELECT `Sites`.`SiteID`, `Sites`.`SiteName`, `Sites`.`SiteEmail`, `Sites`.`SitePhoneNumber`,
            //  `Sites`.`SiteCountry`, `Sites`.`SiteState`, `Sites`.`SiteCity`, `Sites`.`SiteStreet`, `Sites`.`SiteApartment`,
            //  `Sites`.`SiteZipCode`, `Sites`.`Enabled`, `Sites`.`createdAt`, `Sites`.`updatedAt`,
            //  `EyeCareProviders->Patients`.`UserID`,
            //  `EyeCareProviders`.`UserID`
            // --  COUNT(`EyeCareProviders->Patients`.`UserID`) AS `NumOfPatients`,
            // --  COUNT(DISTINCT(`EyeCareProviders`.`UserID`)) AS `NumOfECP`
            //  FROM `Sites` AS `Sites`
            //  LEFT OUTER JOIN `EyeCareProviders` AS `EyeCareProviders`
            //  ON `Sites`.`SiteID` = `EyeCareProviders`.`SiteID`
            //  LEFT OUTER JOIN `Patients` AS `EyeCareProviders->Patients`
            //  ON `EyeCareProviders`.`UserID` = `EyeCareProviders->Patients`.`DoctorID`
            //  AND (`EyeCareProviders->Patients`.`deletedAt` IS NULL AND `EyeCareProviders->Patients`.`AccountStatus` = 2)
            group: ['SiteID'],
            attributes: {
                include: [
                    [models.sequelize.literal('COUNT(DISTINCT(Devices.DeviceID))'), 'NumOfDevices'],
                    [models.sequelize.literal('COUNT(DISTINCT(EyeCareProviders.UserID))'), 'NumOfECP']
                ]
            }
        });


        res.send(sites);

    },

    getAllCustomers: async (req, res, next) => {

        let customers = await models.Customers.findAll({
            include: [
                {
                    model: models.EyeCareProviders,
                    attributes: [],
                    required: false,// true is inner join instead  of left join
                    include: [],
                },
                {
                    model: models.Devices,
                    attributes: ['CustomerID'],
                    required: false,// true is inner join instead  of left join
                    include: [],
                },
                {
                    model: models.Sites,
                    attributes: ['CustomerID'],
                    required: false,// true is inner join instead  of left join
                    include: [],
                }
            ],
/// this query will return same each site for each one of the patient , so we must distinct the ECP
            // SELECT `Sites`.`SiteID`, `Sites`.`SiteName`, `Sites`.`SiteEmail`, `Sites`.`SitePhoneNumber`,
            //  `Sites`.`SiteCountry`, `Sites`.`SiteState`, `Sites`.`SiteCity`, `Sites`.`SiteStreet`, `Sites`.`SiteApartment`,
            //  `Sites`.`SiteZipCode`, `Sites`.`Enabled`, `Sites`.`createdAt`, `Sites`.`updatedAt`,
            //  `EyeCareProviders->Patients`.`UserID`,
            //  `EyeCareProviders`.`UserID`
            // --  COUNT(`EyeCareProviders->Patients`.`UserID`) AS `NumOfPatients`,
            // --  COUNT(DISTINCT(`EyeCareProviders`.`UserID`)) AS `NumOfECP`
            //  FROM `Sites` AS `Sites`
            //  LEFT OUTER JOIN `EyeCareProviders` AS `EyeCareProviders`
            //  ON `Sites`.`SiteID` = `EyeCareProviders`.`SiteID`
            //  LEFT OUTER JOIN `Patients` AS `EyeCareProviders->Patients`
            //  ON `EyeCareProviders`.`UserID` = `EyeCareProviders->Patients`.`DoctorID`
            //  AND (`EyeCareProviders->Patients`.`deletedAt` IS NULL AND `EyeCareProviders->Patients`.`AccountStatus` = 2)
            group: ['CustomerID'],
            attributes: {
                include: [
                    [models.sequelize.literal('COUNT(DISTINCT(Sites.SiteID))'), 'NumOfSites'],
                    [models.sequelize.literal('COUNT(DISTINCT(Devices.DeviceID))'), 'NumOfDevices'],
                    [models.sequelize.literal('COUNT(DISTINCT(EyeCareProviders.UserID))'), 'NumOfECP']
                ]
            }
        });


        res.send(customers);

    },


    getAllECP: async (req, res, next) => {

        let docs = await models.EyeCareProviders.findAll({
            // raw: true,
            // nest:true,
            logging: console.log,
            benchmark: true, // execution time
            include: [
                {
                    model: models.PII, include: [
                        {
                            model: models.Users,
                            attributes: ["Role", "Enabled", "UserID"]
                        }
                    ]

                },
                {
                    model: models.Sites
                },
                // {
                //     model: models.Customers
                // },
                {
                    model: models.Patients,
                    required: false,
                    attributes: [], // exclude all (replace with empty) ///
                    where: {
                        "$Patients.AccountStatus$": 2
                    }
                },

                //{
                //  include:[] //// will include
                //
                //   }

                //[ [models.sequelize.fn('COUNT', models.sequelize.col('Patients.UserID')), 'NumOfPatients']] //// replace all

                // },

            ],
            attributes: {
                include: [[models.sequelize.fn('COUNT', models.sequelize.col('Patients.UserID')), 'NumOfPatients']]

            },
            group: ['UserID'],

        });


        res.send(docs);

    },

    getAllAdmins: async (req, res, next) => {

        let admins = await models.Users.findAll({
            include: [
                {
                    model: models.PII,
                    // required:true
                }
            ],
            where: {Role: JSON.stringify(["EyeSwiftAdmin"])},
            attributes: ["UserID", "Role", "Enabled" , 'TwoFA' , 'AdminPosition', "PassNeedsToChangeAt"],

        });


        res.send(admins);

    },


    // it actually patients
    getAllDevices: async (req, res, next) => {

        let patients = await models.Devices.findAll({
            // where: {AccountStatus: { [models.Sequelize.Op.not]: 1}},  // logged in
            include: [
                {
                    model: models.Sites,
                    attributes: ['SiteName']
                },
                {
                    model: models.Customers,
                    attributes: ['CustomerName']
                },
                {
                    model: models.DeviceErrors,
                },

            ],
        });

        let lastSystemVersion = await models.SystemVersions.gelLatestVersion();


        res.send({Devices: patients , LastSystemVersion: lastSystemVersion});

    },


    editDoctorProfile: async (req, res, next) => {  // by id

        let doctorid = req.params.doctorid
        if (!doctorid)
            throw new Error("no doctorid param")

        let doc = await models.EyeCareProviders.findOne({where: {UserID: doctorid},
            include: [
                {
                    model: models.PII
                }
                ]
        });
        if (!doc)
            throw new Error("no such doctor id")

        //pii model
        let oldEmail = doc.PII.Email
        await doc.PII.update(req.value.userinfo, { context: {req: req, sourceUserID:doctorid}});

        ///doc model
        await doc.update(req.value.userinfo);

        //user model
        if (req.value.userinfo.Enabled) req.value.userinfo.FailedLoginAttempts = 0
        await models.Users.update(req.value.userinfo, {where: {UserID: doctorid}});  // enabled


        // /// send new email in case email account changes
        // if (req.value.userinfo.Email && doc.PII.Email!==oldEmail) {
        //     // req.withoutMail = true; // fixme
        //     if (!(req.withoutMail || req.body.withoutMail)) {  //send email only if user successfully created and withoutMail is null
        //         let emailBody = SEShelper.EmailChangedNotification(doc.PII)
        //         let ansmail = await SEShelper.sendFromEmailAddress( // to doctor,
        //             doc.PII.Email,
        //             'EyeSwift™ Account notification',
        //             emailBody);

        //         if (!(ansmail && ansmail.MessageId))
        //             return res.status(422).send("invalid email request")
        //     }
        // }


        let docs = await models.EyeCareProviders.findOne({
            include: [
                {
                    model: models.PII, include: [
                        {
                            model: models.Users,
                            attributes: ["Role", "Enabled", "UserID"]
                        }
                    ]

                },
                {
                    model: models.Sites
                }
            ],
            where: {UserID: doctorid}

        });


        res.send(docs);

    },

    deleteAdminProfile: async (req, res, next) => {  // by id

        let adminid = req.params.targetadminid
        if (!adminid)
            throw new Error("no adminid")

        let admin = await models.Users.findOne({
            include: [
                {
                    model: models.PII,
                }
            ],
            where: {Role: JSON.stringify(["EyeSwiftAdmin"]), UserID: adminid},
            attributes: ["UserID", "Role", "Enabled"],
            required: true // true is inner join instead  of left join

        });

        if (!admin)
            throw new Error("no such admin")

        let adminPIIObj = admin.PII.toJSON()

        let pii = await admin.PII.destroy({}); // DELETE CASCADE
        if (!pii)
            throw new Error("error admin pii delete")


        // if (!(req.withoutMail || req.body.withoutMail)) {  //send email only if user successfully created and withoutMail is null
        //     let emailBody = SEShelper.deletedAccountNotification(adminPIIObj)
        //     let ansmail = await SEShelper.sendFromEmailAddress( // to doctor,
        //         adminPIIObj.Email,
        //         'EyeSwift™ Account notification',
        //         emailBody);

        //     if (!(ansmail && ansmail.MessageId))
        //         return res.status(422).send("invalid email request")
        // }



        res.send(pii);
    },

    deleteEcpProfile: async (req, res, next) => {  // by id

        let ecpid = req.params.targetecpid
        if (!ecpid)
            throw new Error("no targetecpid")

        let ecp = await models.Users.findOne({
            include: [
                {
                    model: models.PII,
                }
            ],
            where: {Role: JSON.stringify(["Doctor"]), UserID: ecpid},
            attributes: ["UserID", "Role", "Enabled"],
            // required: true // true is inner join instead  of left join

        });

        if (!ecp)
            throw new Error(ecpid)

        let ecpPIIObj = ecp.PII.toJSON()

        let pii = await ecp.PII.destroy({}); // DELETE CASCADE
        if (!pii)
            throw new Error("error ecp pii delete")

        // deleting from eyecareproviders
        // let ecpECPObj = ecp.EyeCareProvider.toJSON()

        // let ECP = await ecp.EyeCareProvider.destroy({}); // DELETE CASCADE
        // if (!ECP)
        //     throw new Error("error ecp ECP delete")

        res.send(pii);
    },

    

    editAdminProfile: async (req, res, next) => {  // by id

        let adminid = req.params.targetadminid
        if (!adminid)
            throw new Error("no adminid")


        let admin = await models.Users.findOne({
            include: [
                {
                    model: models.PII,
                }
            ],
            where: {Role: JSON.stringify(["EyeSwiftAdmin"]), UserID: adminid},
            attributes: ["UserID", "Role", "Enabled"],
            required: true // true is inner join instead  of left join

        });

        if (!admin)
            throw new Error("no such admin")

        // pii model
        let oldEmail = admin.PII.Email
        await admin.PII.update(req.value.userinfo, {context: {req: req, sourceUserID:adminid}});

        // user model
        await models.Users.update(req.value.userinfo, {where: {UserID: adminid}});


        /// send new email in case email account changes
        // if (req.value.userinfo.Email && admin.PII.Email!==oldEmail) {
        //     // req.withoutMail = true; // fixme
        //     if (!(req.withoutMail || req.body.withoutMail)) {  //send email only if user successfully created and withoutMail is null
        //         let emailBody = SEShelper.EmailChangedNotification(admin.PII)
        //         let ansmail = await SEShelper.sendFromEmailAddress( // to doctor,
        //             admin.PII.Email,
        //             'EyeSwift™ Account notification',
        //             emailBody);

        //         if (!(ansmail && ansmail.MessageId))
        //             return res.status(422).send("invalid email request")
        //     }
        // }


        let admins = await models.Users.findOne({
            include: [
                {
                    model: models.PII,
                }
            ],
            where: {Role: JSON.stringify(["EyeSwiftAdmin"]), UserID: adminid},
            attributes: ["UserID", "Role", "Enabled" , 'TwoFA' , "PassNeedsToChangeAt"]
        });


        res.send(admins);
    },

    getAllVersions: async (req, res, next) => {

        let vers = await models.SystemVersions.findAll({
                include: [
                    {
                        model: models.PII,
                        attributes: ["FirstName", "LastName"],
                    }
                ],
            });

        res.send(vers);
    },


    addVersion: async (req, res, next) => {

        req.value.body.AdminID = req.user.UserID
        let ver = await models.SystemVersions.create(req.value.body);


        // fixme s3 updated url validation
        res.send(ver);

    },


    // by id
    EditVersion: async (req, res, next) => {

        let versionid = req.value.body.SystemVersionID

        let ver = await models.SystemVersions.findOne({where: {SystemVersionID: versionid}});
        if (!ver)
            throw new Error("no such version id")

        req.value.body.AdminID = req.user.UserID
        ver = await ver.update(req.value.body)

        // fixme s3 updated url validation

        res.send(ver);

    },


    // delete Version  by id
    delVersion: async (req, res, next) => {

        let versionid = req.params.versionid
        if (!versionid)
            throw new Error("no version id")

        let ver = await models.SystemVersions.findOne({where: {SystemVersionID: versionid}});
        if (!ver)
            throw new Error("no such version id")

        await ver.destroy();
        res.send(ver);

    },

    // delete Customer  by id
    deleteCustomerProfile: async (req, res, next) => {

        let customerid = req.params.targetcustomerid
        if (!customerid)
            throw new Error("no customer id")

        let cus = await models.Customers.findOne({where: {CustomerID: customerid}});
        if (!cus)
            throw new Error("no such customer id")

        await cus.destroy();
        res.send(cus);

    },

    // delete Site  by id
    deleteSiteProfile: async (req, res, next) => {

        let siteid = req.params.targetsiteid
        if (!siteid)
            throw new Error("no site id")

        let site = await models.Sites.findOne({where: {SiteID: siteid}});
        if (!site)
            throw new Error("no such site id")

        await site.destroy();
        res.send(site);

    },

    editClinicProfile: async (req, res, next) => {  // by id

        let siteid = req.params.siteid
        if (!siteid)
            throw new Error("no site id param")

        let site = await models.Sites.findOne({where: {SiteID: siteid}});
        if (!site)
            throw new Error("no such site id")

        await models.Sites.update(req.value.body, {where: {SiteID: siteid}});

        site = await models.Sites.findOne({where: {SiteID: siteid}});
        if (!site)
            throw new Error("no such site id")

        res.send(site);

    },

    editCustomerProfile: async (req, res, next) => {  // by id

        let customerid = req.params.customerid
        if (!customerid)
            throw new Error("no customerid param")

        let customer = await models.Customers.findOne({where: {CustomerID: customerid}});
        if (!customer)
            throw new Error("no such customer id")

        await models.Customers.update(req.value.body, {where: {CustomerID: customerid}});

        customer = await models.Customers.findOne({where: {CustomerID: customerid}});
        if (!customer)
            throw new Error("no such customer id")

        res.send(customer);

    },

    sendMessagesToPatient: async (req, res, next) => {

        let PatientsIDs = req.value.body.PatientsIDs;
        let method = req.value.body.Method;


        let patients = await models.Patients.findAll({
            include: [
                {
                    model: models.PII,
                    include: [
                        {
                            model: models.Users,
                            attributes: ["Role", "Enabled", "UserID"]
                        }
                    ]
                },
            ],
            where: {UserID: {[models.Sequelize.Op.in]: PatientsIDs}} // any patients
        });

        if (!patients || patients.length === 0)
            throw new Error(`no such patients for this doctor`)

        for (let patient of patients) {

            if (method === "SMS" || method === "BOTH") {
                let name = `${patient.PII.FirstName} ${patient.PII.LastName}`
                let smsbody = patientRemainderSMS(name);
                sendToMobile(smsbody , patient.PII.PhoneNumber).
                then(function (ansSms) {
                    if (!(ansSms))
                        console.error("invalid SMS request to patient")
                });
            }

            if (method === "EMAIL" || method === "BOTH") {
                let emailBody = SEShelper.patientReminder(patient.PII);
                let ansmail = SEShelper.sendFromEmailAddress(
                    patient.PII.Email,
                    'Practice makes perfect! EyeSwift™ Reminder',
                    emailBody)
                    .then((ansmail) => {
                        if (!(ansmail && ansmail.MessageId))
                            console.error("invalid EMAIL request to patient")
                        else
                            console.log(" email request sent to patient")
                    });
            }
        }
        res.send(patients);

    },

    sendMessagesToDoctors: async (req, res, next) => {

        let DoctorsIDs = req.value.body.DoctorsIDs;
        let method = req.value.body.Method;


        let doctors = await models.EyeCareProviders.findAll({
            include: [
                {
                    model: models.PII,
                    include: [
                        {
                            model: models.Users,
                            attributes: ["Role", "Enabled", "UserID"]
                        }
                    ]
                },
            ],
            where: {UserID: {[models.Sequelize.Op.in]: DoctorsIDs}} // any doctors
        });

        if (!doctors || doctors.length === 0)
            throw new Error(`no such patients for this doctor`)


        // fixme sms for doctor template
        // for (let doctor of doctors) {
        //
        //     if (method === "SMS"  || method === "BOTH" ){
        //         let name = `${doctor.PII.FirstName} ${doctor.PII.LastName}`
        //         let smsbody = patientRemainderSMS(name);
        //         sendToMobile(smsbody , doctor.PII.PhoneNumber).
        //         then(function (ansSms) {
        //             if (!(ansSms))
        //                 console.error("invalid SMS request to doctor")
        //         });
        //     }
        //
        //     if (method === "EMAIL" || method === "BOTH") {
        //         let emailBody = SEShelper.patientReminder(doctor.PII);
        //         let ansmail = SEShelper.sendFromEmailAddress(
        //             doctor.PII.Email,
        //             'Practice makes perfect! EyeSwift™ Reminder',
        //             emailBody)
        //             .then((ansmail) => {
        //                 if (!(ansmail && ansmail.MessageId))
        //                     console.error("invalid EMAIL request to doctor")
        //                 else
        //                     console.log(" email request sent to doctor")
        //             });
        //     }
        // }

        res.send(doctors);

    },

    updateDevice: async (req, res, next) => {

        let device = await Device.findOne({SerialNumber: req.params.devicesn});
        if (!device) {
            return res.send("Device " + req.params.devicesn + " not exists in DB. ");
        }
        if (!device.UserID) {
            return res.send("Device " + req.params.devicesn + " not linked to patient");
        }
        let pateint = await Patient.findOne({UserID: device.UserID});
        if (!pateint) {
            return res.send("no such patient");
        }

        device = await Device.findOneAndUpdate({DeviceID: device.DeviceID}, req.value.body,
            {new: true, fields: {"Password": 0, Ref: 0}});
        if (!device) {
            throw new Error("bad device update");
        }

        res.send(device);

    },

///////// TO CHECK //////////


    sendTestMail: async (req, res, next) => {

        let to = req.body.to
        let ansmail = await SEShelper.sendFromEmailAddress(to, 'test from server', "hello test")
        res.send(ansmail)

    },


}
