const {Op} = require('sequelize');
const ENV_CONFIG = require('../configurations/env_config');
const {encrypt, decrypt,createTokenSignin, createTokenSignEmail ,createTokenSelfRegistrationEmail, createToken2faSignin ,  verifyTokenSignEmail , CreatePassword} = require('../libs/security');
const SEShelper = require('../libs/email_helper');
const Promise = require('bluebird');
const models  = require('../models'); // this is how you access all sequelize models
const moment = require('moment');
const SMShelper = require('../libs/sms_helper');
const HelperFunctions = require('../libs/helperFunctions');



async function UsersSignIn (req, res, next) {

    const token = createTokenSignin(req.user.UserID, req.user.Role); // Role in db is array
    let  profile = {};
    let dictionary;
    // here we get the user from the db, not token
    if(req.user.Role && req.user.Role.indexOf("Doctor") > -1) {

        let [doctor, patientCount , patientArchiveCount] = await Promise.all([
            models.EyeCareProviders.findOne({where:{UserID : req.user.UserID} , include: [{ model: models.Sites}]}),
            models.Patients.count({where: {DoctorID : req.user.UserID , AccountStatus: { [models.Sequelize.Op.not]: 3}}}),
            models.Patients.count({where: {DoctorID : req.user.UserID, AccountStatus: 3 }}), /// archived patients
        ]);
        if (!doctor)
            throw new Error("error getting doctor")

        let deviceCount = await models.Devices.count({where: {SiteID: doctor.SiteID}})
        if (deviceCount < 1) {
            throw new Error("Doctor's site does not have any registed devices")
        }

        profile = Object.assign(profile , {Doctor : doctor.toJSON() }  , {NumOfPatients:patientCount , NumOfArchivePatients:patientArchiveCount});
    }

    if(req.user.Role && ( (req.user.Role.indexOf("EyeSwiftAdmin") > -1) ||
        (req.user.Role.indexOf("EyeSwiftSuperAdmin") > -1))) {

        let [ecpCount, siteCount , adminsCount ,deviceCount , versionCount, customerCount] = await Promise.all([
            models.EyeCareProviders.count({}),
            models.Sites.count({}),
            models.Users.count({where: { Role: JSON.stringify(["EyeSwiftAdmin"]) } ,}),
            models.Patients.count({where: {AccountStatus: { [models.Sequelize.Op.not]: 1} }}), //logged in //its Devices tab
            models.SystemVersions.count({}),
            models.Customers.count({}),

        ]);

        profile = Object.assign(profile , {NumOfECP : ecpCount }  , {NumOfSites:siteCount} , {NumOfVersions:versionCount} ,
            {NumOfAdmins:adminsCount} , {NumOfDevices:deviceCount} , {NumOfCustomers:customerCount} );
    }

    profile =  Object.assign( profile , req.user )
    res.send({
        status:'PASS',
        updatePassword: req.user.PassNeedsToChange,
        token : token,
        role: req.user.Role,
        profile,
        language: req.user.language,
        dictionary,
    });
}


async function FA2Step (req, res, next)  {

    const token2fa = createToken2faSignin(req.user.UserID, req.user.Role); // Role in db is array , temp token
//        let user  = await models.Users.findOne({where:{ UserID: userPII.UserID }} );
    let code = HelperFunctions.randomNumber(6)
    await models.Users.update( {TwoFaCode:code}, {where: {UserID: req.user.UserID}} )

    await SMShelper.sendToMobile(SMShelper.TwoFaSMS(code) , req.user.PhoneNumber)
    res.send({
        status:'2FA',
        token : token2fa,
        role: req.user.Role,
        profile:{UserID : req.user.UserID}
    });
}



module.exports = {
    //called after passport validation ( req.user is from the db)
    // device login and webapp login are SEPARATED
    // webapp by email , device by username

    signInRouter: async (req, res, next) => {

        /// if is pass validation and he is normal user, then sign in
        if( (req.user.Role && req.user.Role.indexOf("Doctor") > -1) ) {

            return await UsersSignIn(req, res, next)
        }

        /// if he pass validation and he is admin user,  proceed with 2fa code
        if( (req.user.Role && req.user.Role.indexOf("EyeSwiftSuperAdmin") > -1) ||
            (req.user.Role && req.user.Role.indexOf("EyeSwiftAdmin") > -1)) {
            if (req.user.TwoFA)
                return await FA2Step(req, res, next)
            else
                return await UsersSignIn(req, res, next)   /// to remove 2fa
        }

    },

    UsersSignIn:UsersSignIn,


    allUsersForgot: async (req, res, next) => {

        let email = req.value.userinfo.Email
        if (email === ENV_CONFIG.ENV_EMAIL_INFO)
            throw new Error("Forbidden");

        let userPII  = await models.PII.findOne({where:{ Email: email }} );
        if (!userPII) {
            throw new Error(`no such PII user`)
        }

        let user  = await models.Users.scope("withAll").findOne({where:{ UserID: userPII.UserID }} );
        if (!user) {
            throw new Error(`no such user`)
        }

        if (!user.Enabled)
            throw new Error(`user locked`)

        let date = new Date().toISOString()
        let token = createTokenSignEmail(
            userPII.UserID,
            user.Role ,
            date, // payload
            "reset password email"
        );

        await user.update({forgotAt:date})

        let name = userPII.FirstName + " " + userPII.LastName
        let emailBody = SEShelper.resetPasswordPatientEmail(req, token, name)

        let ansmail = await SEShelper.sendFromEmailAddress(
            userPII.Email,
            'EyeSwift account password Reset' ,
            emailBody);

        if (!(ansmail && ansmail.MessageId))
            return res.status(422).send("invalid email request")

        res.send({Token: "sent to mail"});

    },



    // option 1, the email will direct to frontend, front end take the token(from URI fragment), and create a post request that validate the token
    // option 2, the email will direct to api backed that will validate token in params
    resetConfirmWeb: async (req, res, next) => {

        let user_token = req.query.token;
        if (!user_token){
            throw new Error("no user Token");
        }

        // let newPass = req.value.userinfo.Password;
        // if (!newPass){
        //     throw new Error("no new Password");
        // }

        let decodedToken = verifyTokenSignEmail(user_token);

        if (!(decodedToken && decodedToken.as_role &&
            decodedToken.type==="reset password email")) {
             throw new Error("bad Token verification");
        }

        let userid = decodedToken.sub;

        let user  = await models.Users.scope('withAll').findOne({where:{ UserID: userid }} );
        if (!user)
            throw new Error(`no such user`)

        if (!user.forgotAt ||  (user.forgotAt.toISOString() !== decodedToken.payload)) {
            console.error("Expired Token: " + user_token);
            res.set('Content-Type', 'text/html');
            return res.send('<html><body>' + '<h3>' + 'Expired' + '</h3>' + '</body></html>');
        }

        //password change

        let password = CreatePassword(req);

        user =await user.update({Password: password, forgotAt:null , PassNeedsToChangeAt:Date.now() }, {returning: true });   //forgotAt -se null to use once
        if (!user || user._changed.Password===true) // if true , update didnt committed
            throw new Error("error update user pass " + userid);


        ///if you pass in an object or an array, it sets the application/json Content-Type header, and parses that parameter into JSON.
        res.set('Content-Type', 'text/html');
        res.send('<html><body>' +
            '<h3>' +
            'New Temporary Password: ' + password +
            '</h3>' +
            // `<img src="https://liberdi-assets-public.s3-eu-west-1.amazonaws.com/logo.png" alt="liberdi.com">` +
            '</body></html>');
    },

    selfRegisterConfirmWeb: async (req, res, next) => {

        let site, doctorPII, doctor, user;
        try {
            let user_token = req.query.token;
            if (!user_token){
                throw new Error("no user Token");
            }

            let decodedToken = verifyTokenSignEmail(user_token);

            if (!(decodedToken && decodedToken.sub &&
                decodedToken.type==="verify registration")) {
                throw new Error("bad Token verification");
            }


            let Email = decodedToken.sub;

            let pending_user_model = await models.PendingUsers.findOne({where:{ Email }} );
            if (!pending_user_model) {
                throw new Error(`no such pending user`)
            }

            let pending_user = pending_user_model.toJSON()

            let siteid = await models.Counters.getCount("SiteID");

            pending_user.SiteID = siteid
            pending_user.SiteCity = pending_user.City
            pending_user.SiteState = pending_user.State
            pending_user.SiteCountry = pending_user.Country
            pending_user.SiteStreet = pending_user.Street
            pending_user.SiteApartment = pending_user.Apartment
            pending_user.SiteZipCode = pending_user.ZIPCode

            site = await models.Sites.create(pending_user);
            if (!site) throw new Error("Could not create site");

            let doctorid = await models.Counters.getCount("ECPID");
            pending_user.UserID = doctorid
            pending_user.UserIDpk = doctorid

            doctorPII = await models.PII.create(pending_user); // pass options;
            if (!doctorPII) throw new Error("Could not create profile");

            doctor = await models.EyeCareProviders.create(pending_user); // drops additional fields if not in scheme
            if (!doctor) throw new Error("Could not create eye care provider");

            let Password = CreatePassword(req);
            pending_user.Role = ["Doctor"];
            pending_user.Password = Password;
            user = await models.Users.create(pending_user);
            if (!user) throw new Error("Could not create user");

            let ans = Object.assign({}, user.toJSON(), {Doctor: doctor.toJSON()}, {PIIProfile: doctorPII.toJSON()}, {Site: site.toJSON()}, {Password: Password})

            // req.withoutMail = true; // fixme
            if (!(req.withoutMail || req.body.withoutMail)) {  //send email only if user successfully created and withoutMail is null
                let emailBody = SEShelper.CreateNewDoctor(ans)
                let ansmail = await SEShelper.sendFromEmailAddress( // to doctor,
                    Email,
                    'Congratulations for your new EyeSwift™ amblyopia treatment system account',
                    emailBody);

                if (!(ansmail && ansmail.MessageId))
                    return res.status(422).send("invalid email request")

                await pending_user_model.destroy()

                res.send('<html><body>' +
                    '<h3>' + 'Account is activated' + '</h3>' +
                    '<br/>' +
                    '<div>' +'<h4>' + 'Please login into the device' + '</h4>' + '</div>' +
                    '<div>' +'<h4>' + 'then you can login at: ' + ENV_CONFIG.ENV_FRONTEND_URL + '</h4>' + '</div>' +
                    '</body></html>');

            }
        } catch (e) {
            if (site) site.destroy();
            if (doctorPII) doctorPII.destroy();
            if (doctor) doctor.destroy();
            if (user) user.destroy();
            throw e;
        }

    },

    UpdateAllUsersPass: async (req, res, next) => {

        let userid = req.user.UserID /// already escaped
        let user = await models.Users.scope("withAll").findOne({where:{UserID: userid}})
        if (!user) {
            throw new Error("no such user");
        }

        let next3months = moment.utc().startOf('day').add(3, 'months').toDate();
        // next password date that require update
        user = await user.update(
            { Password: req.value.userinfo.newPassword,
                PassNeedsToChangeAt:next3months
            });


        res.send({PassNeedsToChangeAt:next3months , newPassword:true});
    },

    updateLanguage: async (req, res, next) => {

        let userid = req.user.UserID /// already escaped
        let user = await models.Users.findOne({where:{UserID: userid}})
        if (!user) {
            throw new Error("no such user");
        }
        let userupdate = await user.update({language:req.value.body.language});

        res.send({language: userupdate.language})
    },

    RegisterUser: async (req, res, next) => {

            let SiteName = req.value.body.SiteName
            let SiteEmail = req.value.body.SiteEmail

            let MedicalLicense = req.value.body.MedicalLicense

            let Email = req.value.body.Email

            let [siteCount,doctorMDCount,piiCount] = await Promise.all([
                models.Sites.count({where: {[Op.or]: [{SiteName:SiteName},{SiteEmail:SiteEmail}]}}),
                MedicalLicense ? models.EyeCareProviders.count({where: {MedicalLicense:MedicalLicense}}) : -1 ,
                models.PII.count({where: {Email:Email}}),
            ]);

            if (siteCount > 0) throw new Error('Site details already exist (Site name, Site email, Site phone number)')
            if (doctorMDCount > 0) throw new Error('ECP details already exist (Medical license)')
            if (piiCount > 0) throw new Error('Profile details already exist (Email)')

            let token = createTokenSelfRegistrationEmail(Email, "verify registration");

            let name = req.value.body.FirstName + " " + req.value.body.LastName
            let emailBody = SEShelper.selfRegisterEmail(req, token, name)

            let pending_user = await models.PendingUsers.create(req.value.body);
            if (!pending_user) throw new Error("Could not create a pending user");

            let ansmail = await SEShelper.sendFromEmailAddress(
                Email,
                'EyeSwift account registration' ,
                emailBody);

            if (!(ansmail && ansmail.MessageId)) {
                if (pending_user) pending_user.destroy()
                return res.status(422).send("invalid email request")
            }
            res.status(200).send("Check Your Email");

    },
};
