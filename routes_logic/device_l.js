const ENV_CONFIG = require('../configurations/env_config');
const {encrypt, decrypt,createTokenSignin, createTokenSignEmail , verifyTokenSignEmail , CreatePassword} = require('../libs/security');
const SEShelper = require('../libs/email_helper');
const Promise = require('bluebird');
const models  = require('../models'); // this is how you access all sequelize models
const aws_general_helper = require('../libs/aws_general_helper');
const s3Helper = require('../libs/s3_helper');
const moment = require('moment');


module.exports = {

    DoctorsUsersSignIn: async (req, res, next) => {
        const token = createTokenSignin(req.user.UserID, req.user.Role); // Role in db is array
        let S3cred = {AccessKeyId:null , SecretAccessKey:null , SessionToken:null , Expiration:null}
        let device = null
        let  profile
        let doctor

        device = await models.Devices.findOne({where: {SerialNumber: req.value.headers.SerialNumber}})
        if (!device) {
            console.log(`no device SN ${req.value.headers.SerialNumber} found in db`)
            /// create new device
            let deviceid = await models.Counters.getCount("DeviceID");

            let doctor = await models.EyeCareProviders.findOne({where: {UserID: req.user.UserID}})
            if (!doctor){
                console.log(`no doctor with user id ${req.value.headers.SerialNumber} found in db`)
            } else {
                if (doctor.SiteAdmin){
                    let deviceToCreate = Object.assign({} , {DeviceID:deviceid} ,{SerialNumber:req.value.headers.SerialNumber} ,req.value.body, {SiteID: doctor.SiteID})
                    device = await models.Devices.create(deviceToCreate)
                    console.log("device CREATED\n" + JSON.stringify(device))
                } else {
                    throw new Error(`Only site admin is allowed to make first login`)
                }
            }
        }

        if (!device.DateOfActivation) {
            device = await device.update({
                    DateOfActivation: models.sequelize.literal('CURRENT_TIMESTAMP')
                },
                {where: {SerialNumber: req.value.headers.SerialNumber}});
            console.log("updated device DateOfActivation for device: " + device.SerialNumber)
        }

        device = await device.update({
            ESVersion:req.value.userinfo.ESVersion,
            ETFirmwareVersion:req.value.userinfo.ETFirmwareVersion,
            TVVersion:req.value.userinfo.TVVersion,
            EmitterFirmwareVersion:req.value.userinfo.EmitterFirmwareVersion,
            WindowsVersion:req.value.userinfo.WindowsVersion
        });

        let UpdateRequired =await device.calculateAvailableVersion()
        device = device.toJSON()
        device.UpdateRequired = UpdateRequired

        /// create/validate s3 folder for patient
        // await s3Helper.S3createFolder(req.user.UserID)
        //
        // let awsrole = await aws_general_helper.assumeRole(req.user.UserID)
        // if (awsrole)
        //     S3cred = awsrole.Credentials

        profile = Object.assign({}, {FirstName:req.user.FirstName , LastName:req.user.LastName , Gender:req.user.Gender} ,
            Object.assign({}, {Visits:undefined} ), );

        res.send({
            'Token' : token,
            'Role': req.user.Role,
            Device:device,
            Profile:profile,
            // S3cred:S3cred
        });
    },


    setSignedTerm: async (req, res, next) =>{

        let device = await models.Devices.findOne({where: {SerialNumber: req.value.headers.SerialNumber}})
        if (!device) {
            throw new Error(`no device SN ${req.value.headers.SerialNumber} found in db`)
        }

        let patientid = req.params.patientid;
        if (!patientid) {
            throw new Error(`no patientid in request`)
        }

        let patientUser  = await models.Patients.findOne({where:{ PatientID: patientid }} );
        if (!patientUser) {
            throw new Error(`no such patient`)
        }

        if (patientUser.DeviceSN !== device.SerialNumber)
            throw new Error(`device is not linked to patient, linked it on login`)

        let patientupdate = await patientUser.update({TermsOfUseSigned:moment.utc()}); //models.sequelize.literal('CURRENT_TIMESTAMP') ///Sequelize.fn('NOW'), /// moment.utc().format('YYYY-MM-DD HH:mm:ss'),

        res.send({TermsOfUseSigned:patientupdate.TermsOfUseSigned})

    },


    setSystemStatus: async (req, res, next) =>{

        let device = await models.Devices.findOne({where: {SerialNumber: req.value.headers.SerialNumber}})
        if (!device) {
            throw new Error(`no device SN ${req.value.headers.SerialNumber} found in db`)
        }

        let site = await models.Sites.findOne({where: {SiteID: device.SiteID}} );
        if (!site) {
            throw new Error(`Device is not linked to any site`)
        }

        let UserID = req.user.UserID;
        if (!UserID) {
            throw new Error(`no UserID in request`)
        }

        let doctorUser  = await models.Patients.findOne({where:{ UserID: UserID, SiteID: site.SiteID }} );
        if (!doctorUser) {
            throw new Error(`no such Eye care provider`)
        }

        let body = req.value.body.map((val)=>{
            val.DeviceID = device.DeviceID;
            return val
        });

        let deviceErrUpdate = await models.DeviceErrors.bulkCreate(
            body // array of object [{DeviceID,SerialNumber,Error ,ErrorDate}]
        );

        res.send(deviceErrUpdate)
    },



    PatientDeviceForgot: async (req, res, next) => {

        let patientid = req.value.userinfo.PatientID

        let patientUser  = await models.Patients.findOne({where:{ PatientID: patientid }} );
        if (!patientUser) {
            throw new Error(`no such patient`)
        }

        let user  = await models.Users.findOne({where:{ UserID: patientUser.UserID }} );
        if (!user) {
            throw new Error(`no such user`)
        }

        let userPII  = await models.PII.findOne({where:{ UserID: patientUser.UserID }} );
        if (!user) {
            throw new Error(`no such PII user`)
        }

        if (!user.Enabled)
            throw new Error(`user locked`)

        let date = new Date().toISOString()
        let token = createTokenSignEmail(
            patientUser.UserID,
            user.Role ,
            date,
            "reset password email"
        );

        await user.update({forgotAt:date})

        let name = userPII.FirstName + " " + userPII.LastName
        let emailBody = SEShelper.resetPasswordPatientEmail(req, token, name)

        let ansmail = await SEShelper.sendFromEmailAddress(
            userPII.Email,
            'EyeSwift account password Reset' ,
            emailBody);

        if (!(ansmail && ansmail.MessageId))
            return res.status(422).send("invalid email request")

        res.send({Token: "sent to mail"});

    },

    // option 1, the email will direct to frontend, front end take the token(from URI fragment), and create a post request that validate the token
    // option 2, the email will direct to api backed that will validate token in params
    resetConfirm: async (req, res, next) => {

        let user_token = req.query.token;
        if (!user_token){
            throw new Error("no user Token");
        }
        // let newPass = req.value.userinfo.Password;
        // if (!newPass){
        //     throw new Error("no new Password");
        // }

        let decodedToken = verifyTokenSignEmail(user_token);

        if (!(decodedToken && decodedToken.as_role && decodedToken.as_role.includes("Patient") &&
            decodedToken.type==="reset password email")) {
            throw new Error("bad Token verification");
        }

        let userid = decodedToken.sub;

        let user  = await models.Users.scope('withAll').findOne({where:{ UserID: userid }} );
        if (!user)
            throw new Error(`no such user`)

        if (!user.forgotAt ||  (user.forgotAt.toISOString() !== decodedToken.payload)) {
            console.error("Expired Token: " + user_token);
            res.set('Content-Type', 'text/html');
            return res.send('<html><body>' + '<h3>' + 'Expired' + '</h3>' + '</body></html>');
        }

        //password change

        let password = CreatePassword(req);

        user =await user.update({Password: password , forgotAt:null , PassNeedsToChangeAt:null}, {returning: true });  //forgotAt -se null to use once
        if (!user || user._changed.Password===true) // if true , update didnt committed
            throw new Error("error update user pass " + userid);


        ///if you pass in an object or an array, it sets the application/json Content-Type header, and parses that parameter into JSON.
        res.set('Content-Type', 'text/html');
        res.send('<html><body>' +
            '<h3>' +
            'New Temporary Password: ' + password +
            '</h3>' +
            // `<img src="https://liberdi-assets-public.s3-eu-west-1.amazonaws.com/logo.png" alt="liberdi.com">` +
            '</body></html>');
    },


};
