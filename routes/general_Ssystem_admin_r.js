// const router = require('express').Router();
const router = require('express-promise-router')();

const general_admin_l = require('../routes_logic/general_admin_l');
const {PassportJWTAuth, IdCompare  , checkRole} = require('../libs/passport');
const {GeneralValidateBody, GeneralSchemas } = require('../libs/general_admin_validations_helper');
const {DeviceValidateBody, DeviceSchemas} = require('../libs/device_validations_helper');
const {ClinicValidateBody, ClinicSchemas, CustomerSchemas} = require("../libs/site_validations_helper");
const {PatientValidateQuery, PatientValidateBody, PatientSchemas  } = require('../libs/patient_validations_helper');

const {ValidateInfo, ValidateBody,  UserSchemas } = require('../libs/user_validations_helper');

// admin sign-up
router.post('/eyeSwiftAdmins/:adminid/admins/signup',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , [ "EyeSwiftSuperAdmin"] , true ),
    ValidateInfo(UserSchemas.AdminSignUpSchema),
    general_admin_l.AdminSignUp);

router.post('/eyeSwiftAdmins/:adminid/site/signup',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , [ "EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    ClinicValidateBody(ClinicSchemas.signupSchema),
    general_admin_l.SiteSignUp);

router.post('/eyeSwiftAdmins/:adminid/customer/signup',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , [ "EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    ClinicValidateBody(CustomerSchemas.signupSchema),
    general_admin_l.CustomerSignUp);

// doctor sign-up
router.post('/eyeSwiftAdmins/:adminid/doctors/signup',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin" , "EyeSwiftAdmin"] , true ),
    ValidateInfo(UserSchemas.DoctorSignupSchema),
    general_admin_l.DoctorSignUp);

// admin edit
router.post('/eyeSwiftAdmins/:adminid/admins/:targetadminid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin",] , true ),
    ValidateInfo(UserSchemas.AdminUpdateSchema),
    general_admin_l.editAdminProfile);

// admin delete
router.delete('/eyeSwiftAdmins/:adminid/admins/:targetadminid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin"] , true ),
    general_admin_l.deleteAdminProfile);

// customer delete
router.delete('/eyeSwiftAdmins/:adminid/customers/:targetcustomerid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin"] , true ),
    general_admin_l.deleteCustomerProfile);

// site delete
router.delete('/eyeSwiftAdmins/:adminid/sites/:targetsiteid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin"] , true ),
    general_admin_l.deleteSiteProfile);

// doctor delete
router.delete('/eyeSwiftAdmins/:adminid/doctors/:targetecpid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin"] , true ),
    general_admin_l.deleteEcpProfile);

// doctor edit
router.post('/eyeSwiftAdmins/:adminid/doctors/:doctorid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    ValidateInfo(UserSchemas.DoctorUpdateSchema),
    general_admin_l.editDoctorProfile);

router.post('/eyeSwiftAdmins/:adminid/general/d_update/init',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.d_u_init);


router.post('/eyeSwiftAdmins/:adminid/general/d_update/add',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    GeneralValidateBody(GeneralSchemas.d_updateSchema),
    general_admin_l.d_u_add);

// get all
router.get('/general/d_update',
    PassportJWTAuth,
    (req, res, next) => checkRole(req, res, next, ["EyeSwiftAdmin", "ServiceProviderAdmin", "ClinicAdmin", "Doctor"]) , //guard only valid token role
    general_admin_l.d_u_getAll);


router.get('/eyeSwiftAdmins/:adminid/sites/getAllSites',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.getAllSites);

router.get('/eyeSwiftAdmins/:adminid/customers/getAllCustomers',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.getAllCustomers);

router.get('/eyeSwiftAdmins/:adminid/versions/getAllVersions',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.getAllVersions);

router.post('/eyeSwiftAdmins/:adminid/versions/addNewVersion',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    GeneralValidateBody(GeneralSchemas.addVersionsSchema),
    general_admin_l.addVersion);

router.post('/eyeSwiftAdmins/:adminid/versions/editVersion',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    GeneralValidateBody(GeneralSchemas.EditVersionsSchema),
    general_admin_l.EditVersion);

router.delete('/eyeSwiftAdmins/:adminid/versions/:versionid',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.delVersion);

router.get('/eyeSwiftAdmins/:adminid/doctors/getAllECP',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.getAllECP);

router.get('/eyeSwiftAdmins/:adminid/admins/getAllAdmins',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.getAllAdmins);

router.get('/eyeSwiftAdmins/:adminid/devices/getAllDevices',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    general_admin_l.getAllDevices);


//update site
router.post('/eyeSwiftAdmins/:adminid/sites/:siteid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    ClinicValidateBody(ClinicSchemas.updateSchema),
    general_admin_l.editClinicProfile);

//update customer
router.post('/eyeSwiftAdmins/:adminid/customers/:customerid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    ClinicValidateBody(CustomerSchemas.updateSchema),
    general_admin_l.editCustomerProfile);

// send message to patients
router.post('/eyeSwiftAdmins/:adminid/patients/sendMessages',  //get value in req.params.patientid
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    PatientValidateBody(PatientSchemas.sendMessagesSchema), // pii
    general_admin_l.sendMessagesToPatient);

// send message to Doctors
router.post('/eyeSwiftAdmins/:adminid/doctors/sendMessages',  //get value in req.params.patientid
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"adminid" , ["EyeSwiftSuperAdmin", "EyeSwiftAdmin"] , true ),
    GeneralValidateBody(GeneralSchemas.sendMessagesDoctorsSchema), // pii
    general_admin_l.sendMessagesToDoctors);

router.get('/general/version', (req, res, next) => {
    return res.send(require('../app').info)

});

module.exports = router;
