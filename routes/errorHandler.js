const StringBuilder = require("string-builder");
const moment = require('moment');
const util = require('util');
// this function called at the end of the routes middleware and catch the error
const Global_ERR_H  = function (err, req, res, next) {

    if(! err) return;
    if (typeof err === 'string' || err instanceof String){
        err = {message:err}
    }

    let sb = new StringBuilder();
    sb.append(' ------##### - ERROR START - #####-------');
    sb.appendLine("error time :"+ moment.utc().format())
    // sb.appendLine(req.protocol + '://' + req.get('host') + req.baseUrl + req.path)
    sb.appendLine(req.method+ ':'+ req.baseUrl + req.path)
    sb.appendLine("Params: "+ JSON.stringify(req.params,null,2))
    sb.appendLine(req.method==="GET"? "Query: "+JSON.stringify(req.query,null , 2) : "Body: " + JSON.stringify(req.body,null,2))

    if (err.type === 'entity.parse.failed') {
        sb.appendLine('bad body');
        sb.appendLine(err.message);
        sb.appendLine(err.body);
        res.status(400).send({error :'parse syntax error', message: err.message , body: err.body});
    }

    else if (err.message.startsWith("unauthorized")) {
        sb.appendLine(err.message);
        res.status(401).send({error :'unauthorized', message: err.message});
    }

    else if (err.message.startsWith("bad UserID")) {
        sb.appendLine(err.message);
        res.status(401).send({error :'unauthorized', message: err.message});
    }



    else if (err.isJoi) {
        sb.appendLine('joi pre validation violation');
        sb.appendLine(err.message);
        if( err.details) {
            for (let errdetails of err.details){
                errdetails.type = undefined ; errdetails.context = undefined}
        }
        res.status(400).send({error :'joi pre validation validation', message: err.message , body: err.details });
    }


    else if (err.name === 'MongoError' && err.code === 11000) {
        sb.appendLine('mongo Duplicate key error');
        sb.appendLine(err.message);
        res.status(409).send({error :'MongoError Duplicate key', message: err.message});
    }
    else if (err.name === 'ValidationError'){
        sb.appendLine('mongo error validation');
        let on=[]
        for (let e in err.errors){ //this is for in , iterate on keys name of object
            let ob = {}
            ob[e] = err.errors[e].kind
            on.push( ob)
        }
        sb.appendLine(err.message);
        sb.append(JSON.stringify(on))
        res.status(409).send({error :'Mongo Validation Error', message: err.message, body:on});

    }

    else if (err.name === 'SequelizeUniqueConstraintError'){
        sb.appendLine('SQL error validation');
        sb.append(JSON.stringify(err.errors))
        let on=[]
        for (let e in err.errors){ //this is for in , iterate on keys name of object
            let ob = {}
            ob['message'] = err.errors[e].message
            on.push( ob)
        }
        res.status(409).send({error :'SQL Validation Error', message: err.message, body:on});

    }

    else if (err) {
        sb.appendLine('unfamiliar error(print stack): ---');
        sb.appendLine(util.inspect(err, {showHidden: false, depth: null}));
        sb.appendLine('------ end stack  ');
        res.status(422).send({error: err.name||"error" , message : err.message  });
    }
    sb.appendLine(' -------##### ERROR END #####---------');

    //// no stacktraces leaked to user unless in development environment
    console.error(sb.toString());
};

//Warning: Using `'uncaughtException'` correctly
// process.on('uncaughtException', function(error) {
//     console.log("got uncaught exception: \n "+error)
// });

module.exports = Global_ERR_H;
