const router = require('express-promise-router')();
const {PassportJWTAuth, PassportLocalAuth,PassportJWT2FAAuth , IdCompare  , checkRole} = require('../libs/passport');
const users_l = require('../routes_logic/user_l');
const {ValidateInfo, UserSchemas, ValidateBody} = require('../libs/user_validations_helper');
const rateLimit = require("express-rate-limit");


///https://www.npmjs.com/package/rate-limit-mongo// fixme
const loginLimiterFailedRequests = rateLimit({
    windowMs: 5 * 60 * 1000, // 5 min window
    max: 15, // start blocking after X requests
    skipSuccessfulRequests : true,
    message:
        "Too many failed requests created from this IP, please try again after 5 min"
});


const loginLimiterAllRequests = rateLimit({
    windowMs: 5 * 60 * 1000, // 5 min window
    max: 15, // start blocking after X requests
    skipSuccessfulRequests : false,
    message:
        "Too many requests created from this IP, please try again after 5 min"
});


// all web users login
router.post('/signin',
    loginLimiterAllRequests, // must be first middleware
    ValidateInfo(UserSchemas.AllWebUsersSignInSchema),
    PassportLocalAuth,
    (req, res, next) => checkRole(req, res, next, ["EyeSwiftSuperAdmin" , 'EyeSwiftAdmin'  , "Doctor" ] , true , true),
    users_l.signInRouter);

// all web users login
router.post('/signin/2fa',
    loginLimiterAllRequests, // must be first middleware
    ValidateInfo(UserSchemas.twoFAUsersSignInSchema),
    PassportJWT2FAAuth,
    users_l.UsersSignIn);


// // all users(web users) forgot
router.post('/forgetPass',
    loginLimiterAllRequests,
    ValidateInfo(UserSchemas.webForgotSchema),
    users_l.allUsersForgot);

// // all users(web users) forgot reset, from email
router.get('/forgetPass/ResetConfirm',
    loginLimiterAllRequests,
    users_l.resetConfirmWeb);

// all web users change pass
router.post('/users/:userid/changePass',
    loginLimiterAllRequests,
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next, "userid" ,['EyeSwiftSuperAdmin', 'EyeSwiftAdmin' , 'Doctor'] , true),
    ValidateInfo(UserSchemas.ChangePassSchema), // pii
    users_l.UpdateAllUsersPass);

router.post('/users/:userid/changeLanguage',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"userid" , ['EyeSwiftSuperAdmin', 'EyeSwiftAdmin' , 'Doctor'] , true ),
    ValidateBody(UserSchemas.ChangeLanguageSchema), // language
    users_l.updateLanguage);

router.post('/users/self/signup',
    ValidateBody(UserSchemas.registration_schema),
    users_l.RegisterUser);

router.get('/users/self/Confirm',
    loginLimiterAllRequests,
    users_l.selfRegisterConfirmWeb);

module.exports = router;
