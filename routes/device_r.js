const router = require('express-promise-router')();
const device_l = require('../routes_logic/device_l');
const {DeviceValidateBody, ValidateHeader , HeadersSchemas ,DeviceSchemas} = require('../libs/device_validations_helper');
const {PassportJWTAuth, PassportDeviceLocalAuth, IdCompare  , checkRole} = require('../libs/passport');
const {ValidateInfo, UserSchemas , ValidateBody} = require('../libs/user_validations_helper');
const rateLimit = require("express-rate-limit");

///https://www.npmjs.com/package/rate-limit-mongo// fixme
const loginLimiterFailedRequests = rateLimit({
    windowMs: 5 * 60 * 1000, // 5 min window
    max: 15, // start blocking after X requests
    skipSuccessfulRequests : true,
    message:
        "Too many failed requests created from this IP, please try again after 5 min"
});


const loginLimiterAllRequests = rateLimit({
    windowMs: 5 * 60 * 1000, // 5 min window
    max: 15, // start blocking after X requests
    skipSuccessfulRequests : false,
    message:
        "Too many requests created from this IP, please try again after 5 min"
});

// all users login
router.post('/signin',
    loginLimiterAllRequests, // must be first middleware
    ValidateInfo(UserSchemas.AllDeviceUsersSignInSchema),
    ValidateHeader(HeadersSchemas),
    PassportDeviceLocalAuth,
    (req, res, next) => checkRole(req, res, next, ["Doctor"] , true , true) ,
    device_l.DoctorsUsersSignIn);


router.post('/:patientid/setSignedTerm',
    ValidateHeader(HeadersSchemas),
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"patientid" , ["Patient"] , true ),
    device_l.setSignedTerm);


router.post('/:userid/setSystemStatus',
    ValidateHeader(HeadersSchemas),
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"userid" , ["Doctor"] , true ),
    DeviceValidateBody(DeviceSchemas.systemStatusSchema),
    device_l.setSystemStatus);


// router.post('/:patientid/UpdateTreatment',
//     ValidateHeader(HeadersSchemas),
//     PassportJWTAuth,
//     (req, res, next) => IdCompare(req, res, next ,"patientid" , ["Patient"] , true ),
//     DeviceValidateBody(DeviceSchemas.deviceTreatmentSchema),
//     device_l.UpdateTreatment);


// all patient forgot
router.post('/forgetPass',
    loginLimiterAllRequests,
    ValidateInfo(DeviceSchemas.PatientForgotSchema),
    device_l.PatientDeviceForgot);

// all patients forgot reset
router.get('/forgot/ResetConfirm',
    loginLimiterAllRequests,
    device_l.resetConfirm);



router.get('/devices/general/version',
    loginLimiterAllRequests,
    (req, res, next) => res.send(require('../app').info));


module.exports = router;
