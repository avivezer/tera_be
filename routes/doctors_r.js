const router = require('express-promise-router')();
const {PassportJWTAuth , IdCompare  , checkRole} = require('../libs/passport');
const doctor_l = require('../routes_logic/doctors_l');
const {PatientValidateQuery, PatientValidateBody, PatientSchemas , VisitSchemas } = require('../libs/patient_validations_helper');
const {ValidateInfo, UserSchemas , ValidateBody} = require('../libs/user_validations_helper');


// patient signup
router.post('/eyeCareProviders/:doctorid/patients/signup',  // will link patient to this doctor
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    ValidateInfo(UserSchemas.PatientSignupSchema), // pii
    PatientValidateBody(PatientSchemas.signupSchema),  // medical , with visit info
    doctor_l.PatientSignUp);

 // doctor - get my profile
router.get('/eyeCareProviders/:doctorid/profile',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    doctor_l.GetProfileSelfDoctor);

// // doctor - update profile field
// router.post('/eyeCareProviders/:doctorid/profile',
//     PassportJWTAuth,
//     (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
//     ValidateBody(UserSchemas.DoctorUpdateSchema),
//     doctor_l.UpdateSelfDoctor);


// get - get patient profile
router.get('/eyeCareProviders/:doctorid/patients/:patientid/profile',  //get value in req.params.patientid
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    doctor_l.GetMyPatientProfile);

router.get('/eyeCareProviders/:doctorid/patients/:patientid/getLastVisit',  //get value in req.params.patientid
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    doctor_l.GetLastVisit);


// update patient profile
router.post('/eyeCareProviders/:doctorid/patients/:patientid/profile',  //get value in req.params.patientid
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    ValidateInfo(UserSchemas.PatientUpdateSchema), // pii
    PatientValidateBody(PatientSchemas.UpdateSchemaPatient), // pii
    doctor_l.UpdateMyPatientProfile);

// delete patient profile
router.delete('/eyeCareProviders/:doctorid/patients/:patientid/profile',  //get value in req.params.patientid
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    doctor_l.deletePatientProfile);

// send message to patients
router.post('/eyeCareProviders/:doctorid/patients/sendMessages',  //get value in req.params.patientid
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    PatientValidateBody(PatientSchemas.sendMessagesSchema), // pii
    doctor_l.sendMessagesToPatient);


//  get doctor patients
router.get('/eyeCareProviders/:doctorid/getallpatients',
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    doctor_l.GetAllMyPatients);


router.post('/eyeCareProviders/:doctorid/patients/:patientid/addNewVisit',  // will link patient to this doctor
    PassportJWTAuth,
    (req, res, next) => IdCompare(req, res, next ,"doctorid" , ["Doctor"] , true ),
    PatientValidateBody(VisitSchemas.addNewVisit),  // medical , with visit info
    doctor_l.addNewVisit);

module.exports = router;
