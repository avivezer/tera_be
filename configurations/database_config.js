// get from node .envlocal
/// must contain object name by current NODE running envierment
const path = require('path')
const ENV_CONFIG = require(path.resolve(__dirname, "env_config.js"));


module.exports = {

  developmentlocal: {
    username: ENV_CONFIG.ENV_DB_USERNAME,
    password: ENV_CONFIG.ENV_DB_PASSWORD,
    database: ENV_CONFIG.ENV_DB_NAME,
    host: ENV_CONFIG.ENV_DB_ENDPOINT,
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      bigNumberStrings: true,
      // ssl: true // add ca

    },
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    // benchmark:false
    logging: false   // Prevent Sequelize from outputting SQL to the console on execution

  },

  development: {
    username: ENV_CONFIG.ENV_DB_USERNAME,
    password: ENV_CONFIG.ENV_DB_PASSWORD,
    database: ENV_CONFIG.ENV_DB_NAME,
    host: ENV_CONFIG.ENV_DB_ENDPOINT,
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      bigNumberStrings: true,
      // ssl: true // add ca

    },
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    // benchmark:false
    logging: false   // Prevent Sequelize from outputting SQL to the console on execution

  },

  staging: {
    username: ENV_CONFIG.ENV_DB_USERNAME,
    password: ENV_CONFIG.ENV_DB_PASSWORD,
    database: ENV_CONFIG.ENV_DB_NAME,
    host: ENV_CONFIG.ENV_DB_ENDPOINT,
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      bigNumberStrings: true,
      // ssl: true // add ca

    },
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    // benchmark:false
    logging: false   // Prevent Sequelize from outputting SQL to the console on execution


  },

  production: {
    username: ENV_CONFIG.ENV_DB_USERNAME,
    password: ENV_CONFIG.ENV_DB_PASSWORD,
    database: ENV_CONFIG.ENV_DB_NAME,
    host: ENV_CONFIG.ENV_DB_ENDPOINT,
    port: 3306,
    dialect: 'mysql',
    dialectOptions: {
      bigNumberStrings: true,
      // ssl: true // add ca

    },
    pool: {
      max: 10,
      min: 0,
      acquire: 30000,
      idle: 10000
    },
    // benchmark:false
    logging: false   // Prevent Sequelize from outputting SQL to the console on execution

  }



};
