const dotenv = require('dotenv');
const path = require('path')

//dotenv gets called only on local
//dotenv wont override exciting parameters
let dotenvPath
if (process.env.NODE_ENV === "production")
    dotenvPath = '.envprod'

else if (process.env.NODE_ENV === "staging")
    dotenvPath = '.envstaging'

else if (process.env.NODE_ENV === "development")
    dotenvPath = '.envdev'

else if (process.env.NODE_ENV === "developmentlocal")
    dotenvPath = '.envlocal'

else {
    console.log("node env is "+ process.env.NODE_ENV)
    throw new Error("NODE_ENV not defined for.env file")
}


const result = dotenv.config({path: path.resolve(__dirname, dotenvPath)});

if (result.error) {
    if (result.error.code === "ENOENT") {
        console.log("couldn't detect .env file, using default env variables")
    } else {
        throw result.error;
    }
}


console.log("env vars AFTER reading .env (.env not replace existing vars) :")
for (let e of Object.entries(process.env)){
    if (! e[0].startsWith("npm") && !e[0].startsWith("Path") )
        console.log(e)
}



module.exports = {
    ENV_NODE_ENV : process.env.NODE_ENV,
    ENV_DB_ENDPOINT: process.env.ENV_DB_ENDPOINT || 'localhost',
    ENV_DB_USERNAME: process.env.ENV_DB_USERNAME,
    ENV_DB_PASSWORD: process.env.ENV_DB_PASSWORD,
    ENV_DB_NAME: process.env.ENV_DB_NAME,
    ENV_IOT_ENDPOINT: process.env.ENV_IOT_ENDPOINT,
    ENV_AWS_ACCESS_KEY_ID: process.env.ENV_AWS_ACCESS_KEY_ID,
    ENV_AWS_ACCESS_KEY_SECRET:process.env.ENV_AWS_ACCESS_KEY_SECRET,
    ENV_AWS_REGION : process.env.ENV_AWS_REGION,
    ENV_S3_BUCKET_NAME:  process.env.ENV_S3_BUCKET_NAME,
    ENV_S3_Versions_BUCKET_NAME:  process.env.ENV_S3_Versions_BUCKET_NAME,

    ENV_S3_Role:process.env.ENV_S3_Role,
    ENV_JWT_SECRET:process.env.ENV_JWT_SECRET,
    ENV_SQS_ALGO_OUT:process.env.ENV_SQS_ALGO_OUT ,
    ENV_SQS_ALGO_IN:process.env.ENV_SQS_ALGO_IN ,
    ENV_FRONTEND_URL:process.env.ENV_FRONTEND_URL,
    ENV_EMAIL_INFO: process.env.ENV_EMAIL_INFO,
    ENV_SUADMIN_PASSWORD: process.env.ENV_SUADMIN_PASSWORD,
};


